// Run time 492 ms

class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& A) {
        vector<unordered_map<long long, int>> count(A.size());
        int ans = 0;
        for (int i = 1; i < A.size(); ++i) {
            for (int j = 0; j < i; ++j) {
                long long left = A[j], right = A[i];
                if (right - left > INT_MAX || right - left < INT_MIN) continue;
                auto rnt = count[j].find(right - left);
                if (rnt != count[j].end()) {
                    count[i][right - left] += rnt->second + 1;
                    ans += count[j][right - left];
                }
                else count[i][right - left] += 1;
            }
        }
        return ans;
    }
};
