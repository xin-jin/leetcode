// Run time 58ms

class Solution {
public:
    int findPoisonedDuration(vector<int>& timeSeries, int duration) {
		pair<int, int> cur = {0, 0}; // first is start time, second is end time
		int rnt = 0;
		for (int i = 0; i != timeSeries.size(); ++i) {
			if (timeSeries[i] <= cur.second) {
				cur.second = timeSeries[i] + duration;
			}
			else {
				rnt += cur.second - cur.first;
				cur = { timeSeries[i], timeSeries[i] + duration };
			}
		}

		return rnt + cur.second - cur.first;
    }
};
