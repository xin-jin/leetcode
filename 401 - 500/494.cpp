// Run time 30ms

class Solution {
public:
    int findTargetSumWays(vector<int>& nums, int S) {
		int max_sum = 1000;
		if (S > max_sum || S < -max_sum) return 0;
		vector<int> counts(max_sum * 2 + 1, 0);
		counts[max_sum] = 1;
		for (int x : nums) {
			vector<int> new_counts(max_sum * 2 + 1, 0);
			for (int i = 0; i != max_sum * 2 + 1; ++i) {
				if (i - x >= 0) {
					new_counts[i] += counts[i - x];
				}
				if (i + x <= max_sum * 2) {
					new_counts[i] += counts[i + x];
				}
			}
			swap(counts, new_counts);
		}
		return counts[S + 1000];
    }
};
