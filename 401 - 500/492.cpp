// Run time 3ms

class Solution {
public:
    vector<int> constructRectangle(int area) {
		int root = round(sqrt(area));
		while (area % root != 0) --root;
		return { area / root, root };
    }
};
