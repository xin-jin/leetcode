// Run time 389ms
// Slower than many solutions that do simple prunning, but I think it's because the test cases are too weak.

class Solution {
public:
    bool canCross(vector<int>& stones) {
		int n = stones.size();
		vector<int> largest_jump(n, 0);
		vector<unordered_set<int>> jump_size(n);

		jump_size[1].insert(1);		
		if (n == 1) return true;
		if (n == 2) {
			if (stones[1] == 1) return true;
			else return false;
		}
		largest_jump[1] = 1;
		
		for (int i = 1; i != n; ++i) {
			for (int j = i+1; j < n; ++j) {
				int diff = stones[j] - stones[i];
				if (diff > largest_jump[i]+1) break;
				for (int k = diff-1; k <= diff+1; ++k) {
					if (jump_size[i].find(k) != jump_size[i].end()) {
						if (j == n-1) return true;
						jump_size[j].insert(diff);
						largest_jump[j] = max(largest_jump[j], diff);
					}
				}
			}
		}

		return false;
    }
};

