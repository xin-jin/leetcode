// Run time 12ms

class Solution {
public:
    struct PosInfo {
        int dist;
        string path;
        bool visited = false;
    };

    struct NodeInfo {
        string path;
        int traveled;
        pair<int, int> pos;
        NodeInfo(string path_, int traveled_, pair<int, int> pos_): path(path_), traveled(traveled_), pos(pos_){}
    };

    string findShortestWay(vector<vector<int>>& maze, vector<int>& ball, vector<int>& hole) {
        if (!maze.size() || !maze[0].size()) return "impossible";
        height = maze.size(), width = maze[0].size();
        vector<vector<PosInfo>> pos_info(height, vector<PosInfo>(width));
        queue<NodeInfo> frontier;
        frontier.push(NodeInfo("", 0, { ball[0], ball[1] }));
        while (!frontier.empty()) {
            auto cur = frontier.front();
            frontier.pop();
            if (cur.pos.first == hole[0] && cur.pos.second == hole[1]) return cur.path;
            PosInfo &cur_info = pos_info[cur.pos.first][cur.pos.second];
            if (cur_info.visited) {
                if (cur.traveled > cur_info.dist || (cur.traveled == cur_info.dist && cur.path >= cur_info.path)) continue;
            }
            cur_info.visited = true;
            cur_info.dist = cur.traveled;
            cur_info.path = cur.path;

			rolls_to(maze, cur, hole, frontier, 1, 0, 'd');
			rolls_to(maze, cur, hole, frontier, 0, -1, 'l');
			rolls_to(maze, cur, hole, frontier, 0, 1, 'r');
			rolls_to(maze, cur, hole, frontier, -1, 0, 'u');
		}

        return right_path;
    }

private:
    string right_path = "impossible";
    int shortest_dist = INT_MAX, height, width;
	void rolls_to(const vector<vector<int>>& maze, const NodeInfo& cur, const vector<int>& hole,
				  queue<NodeInfo>& frontier, int vd, int hd, char label) {
		bool reached = false;
		auto new_pos = cur.pos;
		while (new_pos.first + vd >= 0 && new_pos.first + vd < height &&
			   new_pos.second + hd >= 0 && new_pos.second + hd < width &&
			   maze[new_pos.first+vd][new_pos.second+hd] == 0) {
			new_pos.first += vd;
			new_pos.second += hd;
			if (new_pos.first == hole[0] && new_pos.second == hole[1]) {
				reached = true;
				break;
			}
		}
		int new_dist = cur.traveled + abs(cur.pos.first - new_pos.first) + abs(cur.pos.second - new_pos.second);
		if (!reached) frontier.push(NodeInfo(cur.path + label, new_dist, new_pos));
		else updateBest(cur.path + label, new_dist);
	}

    void updateBest(const string& path, int dist) {
        if (dist < shortest_dist || (dist == shortest_dist && path < right_path)) {
            shortest_dist = dist;
            right_path = path;
        }
    }
};
