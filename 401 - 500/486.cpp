// Run time 3ms

class Solution {
public:
    bool PredictTheWinner(vector<int>& nums) {
		if (!nums.size()) return true;
		vector<vector<pair<int, int>>> scores(nums.size(), vector<pair<int, int>>(nums.size(), {-1, -1}));
		auto score = getScore(nums, scores, 0, nums.size()-1);
		return score.first >= score.second;
    }

private:
	pair<int, int> getScore(vector<int>& nums, vector<vector<pair<int, int>>>& scores, int left, int right) {
		if (left == right) return { nums[left], 0 };
		if (scores[left][right].first != -1) return scores[left][right];
		auto pick_left = getScore(nums, scores, left+1, right);
		auto pick_right = getScore(nums, scores, left, right-1);
		if (pick_left.second + nums[left] >= pick_right.second + nums[right]) {
			scores[left][right] = { pick_left.second + nums[left], pick_left.first };
		}
		else {
			scores[left][right] = { pick_right.second + nums[right], pick_right.first };
		}
		return scores[left][right];
	}
};
