// Run time 3ms

class Solution {
public:
    int longestPalindrome(string s) {
        if (!s.size()) return 0;
        vector<int> chs(128, 0);
        for (int i = 0; i != s.size(); ++i) {
            ++chs[s[i]];
        }
        bool middle = false;
        int rnt = 0;
        for (int i = 0; i != 128; ++i) {
            rnt += chs[i] / 2;
            if (chs[i] & 1) middle = true;
        }
        return rnt*2 + middle;
    }
};
