// Run time 3ms

class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& A) {
		if (A.size() < 3) return 0;
        int last = A[1] - A[0] - 1, len = 0, cnt = 0;
        for (int i = 1; i != A.size(); ++i) {
			int diff = A[i] - A[i-1];
            if (diff == last) {
                ++len;
				if (len >= 3) cnt += len - 2;
            }
            else {
				last = diff;
				len = 2;
            }
        }
        return cnt;
    }
};
