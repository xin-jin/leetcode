// Run Time 92ms

class Solution {
public:
    int coinChange(vector<int>& coins, int amount) {
		vector<int> min_combinations(amount+1, amount+1);
		min_combinations[0] = 0;

		for (auto coin : coins) {
			for (int i = 0; i <= amount - coin; ++i) {
				min_combinations[i+coin] = min(min_combinations[i+coin], min_combinations[i]+1);
			}
		}

		return min_combinations[amount] <= amount ? min_combinations[amount] : -1;
	}
};
