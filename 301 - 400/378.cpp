// Run time 270ms

class Solution {
public:
    int kthSmallest(vector<vector<int>>& matrix, int k) {
		size_t n = matrix.size();
		int last_min;
        vector<int> col_idx(n, 0);
		for (int i = 0; i != k; ++i) {
			int min = numeric_limits<int>::max(), min_idx;
			for (int j = 0; j != n; ++j) {
				if (col_idx[j] < n && matrix[j][col_idx[j]] < min) {
					min = matrix[j][col_idx[j]];
					min_idx = j;
				}
			}
			last_min = min;
			++col_idx[min_idx];
		}

		return last_min;
    }
};
