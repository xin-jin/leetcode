// Run time 8ms

class Solution {
public:
    int maxProfit(vector<int>& prices) {
		int maxp = 0, p_hot = INT_MIN, maxp_hold = INT_MIN;
		for (int p : prices) {
		    int tmp = maxp;
			maxp = max(p_hot, maxp);
			p_hot = maxp_hold + p;
			maxp_hold = max(maxp_hold, tmp - p);
		}

		return max(maxp, p_hot);
    }
};
