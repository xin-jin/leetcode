// Run time 12ms

class Solution {
public:
    string reverseString(string s) {
		for (auto i = s.begin(), j = s.end() - 1; i <= j; ++i, --j) {
			swap(*i, *j);
		}
        return s;
    }
};
