// Run time 7ms

class Solution {
public:
    char findTheDifference(string s, string t) {
        vector<int> count(26, 0);
        for (char c : s) {
            ++count[c-97];
        }
        for (char c : t) {
            if (--count[c-97] < 0) {
                return c;
            }
        }
        return 'a';
    }
};
