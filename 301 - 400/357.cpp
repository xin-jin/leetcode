// Run time 0ms

class Solution {
public:
    int countNumbersWithUniqueDigits(int n) {
		vector<int> counts(11);
		counts[0] = 1;
		counts[1] = 9;
		n = min(n, 10);
		for (int i = 2; i <= n; ++i) {
			counts[i] = counts[i-1] * (11 - i);
		}
		int ans = 0;
		for (int i = 0; i <= n; ++i)
			ans += counts[i];
		return ans;
    }
};
