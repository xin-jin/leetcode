/** Run Time 120ms */

class Solution {
public:
    vector<int> countBits(int num) {
		vector<int> rnt(num+1);
		rnt[0] = 0;
		for (int i = 1; i <= num; ++i) {
			rnt[i] = rnt[i >> 1] + (i & 1);
		}

		return rnt;
	}
};
