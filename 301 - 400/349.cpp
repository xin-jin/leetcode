// Run time 8ms

class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
		vector<int> ans;
		sort(nums1.begin(), nums1.end());
		sort(nums2.begin(), nums2.end());
		auto p1 = nums1.begin();
		auto p2 = nums2.begin();
		while (p1 != nums1.end() && p2 != nums2.end()) {
			if (*p1 < *p2)
				++p1;
			else if (*p1 > *p2)
				++p2;
			else {
				ans.push_back(*p1);
				int v = *p1;
				while (p1 != nums1.end() && *p1 == v)
					++p1;
				while (p2 != nums2.end() && *p2 == v)
					++p2;
			}
		}

		return ans;
    }
};
