// Run time 52ms

class Solution {
public:
    vector<int> largestDivisibleSubset(vector<int>& nums) {
		if (!nums.size()) return vector<int>{};
		sort(nums.begin(), nums.end());
		vector<int> len(nums.size(), 1), last(nums.size(), -1);
		int global_max_len = 1, global_best_last = 0;
		for (int i = 1; i != nums.size(); ++i) {
			int max_len = 0, best_last = -1;
			for (int j = 0; j < i; ++j) {
				if (nums[i] % nums[j] == 0) {
					if (len[j] > max_len) {
						max_len = len[j];
						best_last = j;
					}
				}
			}
			if (best_last != -1) {
				len[i] = max_len + 1;
				last[i] = best_last;
				if (len[i] > global_max_len) {
					global_max_len = len[i];
					global_best_last = i;
				}
			}
		}

		vector<int> ans{nums[global_best_last]};
		while (last[global_best_last] != -1) {
			global_best_last = last[global_best_last];
			ans.push_back(nums[global_best_last]);
		}

		return ans;
    }
};
