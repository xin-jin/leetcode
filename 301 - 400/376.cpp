/**
 *  Run time 0ms
 *  Consider the next element to keep. Easy to see being greedy is the best strategy.
 */

class Solution {
public:
    int wiggleMaxLength(vector<int>& nums) {
		if (nums.size() <= 1) return nums.size();
		int last = nums[1], count = 1 + (nums[1] != nums[0]);
		bool going_up = nums[1] > nums[0];
		for (auto n = nums.cbegin() + 2; n != nums.end(); ++n) {
			if (going_up) {
				if (*n < last) {
					++count;
					going_up = false;
				}
				last = *n;
			}
			else {
				if (*n > last) {
					++count;
					going_up = true;
				}
				last = *n;
			}
		}

		return count;
    }
};
