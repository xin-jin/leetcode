// Run time 40ms

class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {
		unordered_map<int, int> dict;
		for (int i = 0; i != nums.size(); ++i) {
			++dict[nums[i]];
		}
		
		vector<vector<int>> freq(nums.size()+1);
		for (auto i = dict.begin(); i != dict.end(); ++i) {
			freq[i->second].push_back(i->first);
		}
		vector<int> ans;
		int count = 0;
		for (int i = freq.size() - 1; count < k; --i) {
			for (auto& j : freq[i]) {
				ans.push_back(j);
				++count;
				if (count == k) break;
			}
		}
		return ans;
    }
};
