// Run time 0ms

class Solution {
public:
    bool canMeasureWater(int x, int y, int z) {
		if (!z) return true;
		if (x < y) swap(x, y);
		if (!y) {
			if (x == z) return true;
			else return false;
		}
		if (z <= x+y && z % gcd(x, y) == 0) return true;
		else return false;
    }

private:
	int gcd(int x, int y) {
		while (y != 0) {
			x %= y;
			swap(x, y);
		}
		return x;
	}
};
