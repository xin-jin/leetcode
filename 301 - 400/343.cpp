/**
 * Run time 0ms
 * The idea is to note that
 * 1. For every integer n > 4, the optimal product of splitted parts is larger than n itself.
 * 2. If the optimal split of n contains k, then the optimal split must be k + optimal_split(n-k).
 * 3. By induction, one can easily show that the optimal split of every n must contain 3. The proof uses
 *    an implication of rule 1 and rule 2: one of the splitted terms must be 1, 2, or 3.
 *
 * By the above three rules, we can easily give a recursion for n > 4: f(n) = f(n-3) * 3. And thus
 * we deduce that we should just split n into as many 3s as possible (except perhaps one term).
 */

class Solution {
public:
    int integerBreak(int n) {
        if (n <= 4) {
            switch (n) {
            case 2: return 1;
            case 3: return 2;
            case 4: return 4;
            }
        }

		if (n % 3 == 0) return pow(3, n/3);
		if (n % 3 == 1) return pow(3, n/3-1) * 4;
		return pow(3, n/3) * 2;
    }
};
