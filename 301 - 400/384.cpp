// Run time 348ms

class Solution {
public:
    Solution(vector<int> nums): original(nums) {
    }
    
    /** Resets the array to its original configuration and return it. */
    vector<int> reset() {
        return original;
    }
    
    /** Returns a random shuffling of the array. */
    vector<int> shuffle() {
		vector<int> shuffled_array(original);
        for (int j = shuffled_array.size() - 1; j >= 0; --j) {
			int to_switch = rand() % (j+1);
			swap(shuffled_array[to_switch], shuffled_array[j]);
		}

		return shuffled_array;
    }

private:
	vector<int> original;
};

