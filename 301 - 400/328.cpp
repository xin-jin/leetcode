// Run time 20ms

class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {
		ListNode dummy_odd(0), dummy_even(0), *last_odd = &dummy_odd, *last_even = &dummy_even;
		bool odd_now = true;
		while (head) {
			if (odd_now) {
				last_odd->next = head;
				last_odd = head;
			}
			else {
				last_even->next = head;
				last_even = head;
			}
			head = head->next;
			odd_now = !odd_now;
		}
		last_odd->next = dummy_even.next;
		last_even->next = nullptr;
		return dummy_odd.next;
	}
};
