class Solution(object):
    def firstUniqChar(self, s):
        """
        :type s: str
        :rtype: int
        """
        cnt = [[0,0] for i in range(26)]
        for i in range(len(s)):
            ind = ord(s[i]) - 97
            if cnt[ind][0] == 0:
                cnt[ind] = [1, i]
            else:
                cnt[ind][0] += 1
        
        first = len(s)
        for i in range(26):
            if cnt[i][0] == 1 and cnt[i][1] < first:
                first = cnt[i][1]
                
        return first if first < len(s) else -1
