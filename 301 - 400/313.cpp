// Run time 88ms

class Solution {
public:
    int nthSuperUglyNumber(int n, vector<int>& primes) {
		vector<int> next_to_use(primes.size(), 0), ugly{1};
		
		for (int i = 1; i != n; ++i) {
			int ugly_candidate = numeric_limits<int>::max();
			int prime_to_use;
			for (int j = 0; j != primes.size(); j++) {
				int tmp = primes[j] * ugly[next_to_use[j]];
				if (tmp == ugly[i-1]) {
					++next_to_use[j];
					tmp = primes[j] * ugly[next_to_use[j]];
				}
				if (tmp < ugly_candidate) {
					ugly_candidate = tmp;
					prime_to_use = j;
				}
			}
			ugly.push_back(ugly_candidate);
			++next_to_use[prime_to_use];
		}

		return ugly[n-1];
    }
};
