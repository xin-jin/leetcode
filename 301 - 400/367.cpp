// Run time 0ms

class Solution {
public:
    bool isPerfectSquare(int num) {
		if (num == 0) return true;
		double x = 1, prev = 100;
		while (abs(x - prev) >= 1) {
			prev = x;
			x = x - (x - num/x)/2;
		}
		int guess = round(x);
		if (guess*guess == num) return true;
		else return false;
	}
};
