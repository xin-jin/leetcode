// Run time 8ms

class Solution {
public:
    bool increasingTriplet(vector<int>& nums) {
		int min_first = INT_MAX, min_second = INT_MAX;
		for (auto i = nums.begin(); i != nums.end(); ++i) {
			if (*i > min_second) return true;
			min_first = min(min_first, *i);
			if (*i > min_first && *i < min_second) min_second = *i;
		}
		return false;
    }
};
