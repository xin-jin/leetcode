// Run time 8ms

class Solution {
public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
		vector<int> ans;
		sort(nums1.begin(), nums1.end());
		sort(nums2.begin(), nums2.end());
		auto p1 = nums1.begin();
		auto p2 = nums2.begin();
		while (p1 != nums1.end() && p2 != nums2.end()) {
			if (*p1 < *p2)
				++p1;
			else if (*p1 > *p2)
				++p2;
			else {
				ans.push_back(*p1);
				++p1;
				++p2;
			}
		}

		return ans;
    }
};
