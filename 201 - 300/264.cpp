// Run time 26ms
// Seems slow

class Solution {
public:
    int nthUglyNumber(int n) {
        vector<int> primes {2, 3, 5}, ugly {1}, last_ugly {0, 0, 0};
        for (int i = 1; i < n; ++i) {
            int candidate = primes[0] * ugly[last_ugly[0]];
            for (int j = 1; j != 3; ++j) {
                int tmp = primes[j] * ugly[last_ugly[j]];
                if (tmp < candidate)
                    candidate = tmp;
            }
            ugly.push_back(candidate);
            for (int j = 0; j != 3; ++j) {
                if (primes[j] * ugly[last_ugly[j]] == candidate)
                    ++last_ugly[j];
            }
        }
        
        return ugly[n-1];
    }
};
