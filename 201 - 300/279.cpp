// Run time 236ms

class Solution {
public:
    int numSquares(int n) {
		vector<int> cnt(n+1, n+1);
		cnt[0] = 0;
		for (int i = 1; i != n+1; ++i) {
			int tmp = n+1;
			for (int j = 1; j*j <= i; ++j) {
				tmp = min(tmp, cnt[i-j*j]);
			}
			cnt[i] = tmp + 1;
		}
		return cnt[n];
    }
};
