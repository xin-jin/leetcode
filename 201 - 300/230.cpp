// Run time 19ms

class Solution {
public:
    int kthSmallest(TreeNode* root, int k) {
        k_ = k;
        inorder(root);
        return ans_; 
    }
    
private: 
    void inorder(TreeNode* node) {
        if (k_ == 0) return;
        if (node->left) inorder(node->left);
        --k_;
        if (k_ == 0) {
            ans_ = node->val;
            return;
        }
        if (node->right) inorder(node->right);
    }
    int k_;
    int ans_;
};
