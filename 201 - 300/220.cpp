// Run time 48ms

class Solution {
public:
    bool containsNearbyAlmostDuplicate(vector<int>& nums, int k, int t) {
		set<int> numset;
		auto judge = [t, &numset](int val) {
			auto lb = numset.lower_bound(val);
			// There is an easier way: find the lower bound of (val - t), 
			// then you only need to compare the lower_bound with val
			if (lb != numset.end() && ((long long)(*lb) - val) <= t) return true;			
			if (lb != numset.begin() && ((long long)(val) - (*(--lb))) <= t) return true;
			return false;
		};		  
		for (int i = 0; i <= k && i < nums.size(); ++i) {
			if (judge(nums[i])) return true;
			numset.insert(nums[i]);
		}
		for (int i = k+1; i < nums.size(); ++i) {
			numset.erase(nums[i-k-1]);
			if (judge(nums[i])) return true;
			numset.insert(nums[i]);
		}
		return false;
	}
};
