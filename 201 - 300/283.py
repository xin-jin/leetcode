class Solution(object):
    def moveZeroes(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        right_pos = 0
        for i in range(len(nums)):
            if nums[i] is not 0:
                nums[i], nums[right_pos] = nums[right_pos], nums[i]
                right_pos += 1
