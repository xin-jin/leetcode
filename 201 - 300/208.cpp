// Run time 59ms

class TrieNode {
public:
    // Initialize your data structure here.
    TrieNode(): ch(26, nullptr) {}

    bool string_end = false;
    vector<TrieNode*> ch;
};

class Trie {
public:
    Trie() {
        root = new TrieNode();
    }

    // Inserts a word into the trie.
    void insert(string word) {
        TrieNode *cur = root;
        int i;
        for (i = 0; i != word.size(); ++i) {
			auto tmp = cur->ch[word[i] - 'a'];
            if (tmp == nullptr) break;
            else cur = tmp;
        }

        for ( ; i != word.size(); ++i) {
            TrieNode *tmp = new TrieNode;
			cur->ch[word[i] - 'a'] = tmp;
            cur = tmp;
        }
        cur->string_end = true;
    }

    // Returns if the word is in the trie.
    bool search(string word) {
		if (find(word) == nullptr) return false;
        else return find(word)->string_end;
    }

    // Returns if there is any word in the trie
    // that starts with the given prefix.
    bool startsWith(string prefix) {
        return find(prefix) != nullptr;
    }

    TrieNode* find(string prefix) {
        TrieNode *cur = root;
        for (int i = 0; i != prefix.size(); ++i) {
            auto tmp = cur->ch[prefix[i] - 'a'];
            if (tmp == nullptr) return nullptr;
            else cur = tmp;
        }
        return cur;
    }

private:
    TrieNode* root;
};
