// Run time 3ms

class Solution {
public:
    int minSubArrayLen(int s, vector<int>& nums) {
        int n = nums.size();
        if (!n) return 0;
        int head = 0, running_sum = 0, len, min_len = INT_MAX;
        for (int i = 0; i != n; ++i) {
            running_sum += nums[i];
            if (running_sum >= s) {
                while (running_sum >= s) running_sum -= nums[head++];
                len = i - head + 2;
                min_len = min(min_len, len);
            }
        }
        
        return min_len < INT_MAX ? min_len : 0;
    }
};
