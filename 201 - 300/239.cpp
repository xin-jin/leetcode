// Run time 82ms

class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) {
        deque<int> window;
        vector<int> ans;
        if (!nums.size()) return ans;
        for (int i = 0; i != k; ++i) {
            while (!window.empty() && nums[window.back()] <= nums[i])
                window.pop_back();
            window.push_back(i);
        }
        ans.push_back(nums[window.front()]);
        
        for (int i = k; i != nums.size(); ++i) {
            if (window.front() == i - k)
                window.pop_front();
            while (!window.empty() && nums[window.back()] <= nums[i])
                window.pop_back();
            window.push_back(i);
            ans.push_back(nums[window.front()]);
        }
        
        return ans;
    }
};
