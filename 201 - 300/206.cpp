// Run time 8ms

class Solution {
public:
    ListNode* reverseList(ListNode* head) {
		if (!head) return head;
		ListNode *current = nullptr, *ahead = head;
		while (ahead) {
			ListNode *tmp = ahead->next;
			ahead->next = current;
			current = ahead;
			ahead = tmp;
		}

		return current;
    }
};
