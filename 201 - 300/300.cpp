// Run time 6ms

class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
		if (!nums.size()) return 0;
		int longest = 1;
		vector<int> min_last{nums[0]};
		for (int i = 1; i != nums.size(); ++i) {
			auto lb = lower_bound(min_last.begin(), min_last.end(), nums[i]);
			if (lb == min_last.end()) {
				++longest;
				min_last.push_back(nums[i]);
			}
			else *lb = std::min(*lb, nums[i]);
		}

		return longest;
    }
};
