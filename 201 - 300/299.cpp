// Run time 4ms

class Solution {
public:
    string getHint(string secret, string guess) {
        size_t len = secret.size(), bull = 0, cow = 0;
        vector<int> counts(10, 0);
        for (int i = 0; i != len; ++i) {
            ++counts[secret[i]-48];
        }
        for (int i = 0; i != len; ++i) {
            if (secret[i] == guess[i]) {
                ++bull;
                if (counts[guess[i]-48])
                    --counts[guess[i]-48];
                else
                    --cow;
				continue;
            }
            if (counts[guess[i]-48]) {
                --counts[guess[i]-48];
                ++cow;
            }
        }
        return to_string(bull) + "A" + to_string(cow) + "B";
    }
};
