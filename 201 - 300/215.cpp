// Run time 16ms

class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        auto cmp = std::greater<int>();
        priority_queue<int, vector<int>, decltype(cmp)> pq(cmp);
        for (int i = 0; i < k; ++i) {
            pq.push(nums[i]);
        }
        
        for (int i = k; i != nums.size(); ++i) {
            if (nums[i] > pq.top()) {
                pq.pop();
                pq.push(nums[i]);
            }
        }
        
        return pq.top();
    }
};
