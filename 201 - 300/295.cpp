// Run time 119ms

class MedianFinder {
public:
    MedianFinder(): rightpq_(greater<int>()) {}

    // Adds a number into the data structure.
    void addNum(int num) {
		if (leftpq_.size() == 0) leftpq_.push(num);
        else if (leftpq_.size() == rightpq_.size()) {
			if (num < rightpq_.top()) leftpq_.push(num);
			else {
				leftpq_.push(rightpq_.top());
				rightpq_.pop();
				rightpq_.push(num);
			}
		}
		else {
			if (num >= leftpq_.top()) rightpq_.push(num);
			else {
				rightpq_.push(leftpq_.top());
				leftpq_.pop();
				leftpq_.push(num);
			}
		}
    }

    // Returns the median of current data stream
    double findMedian() {
        if (leftpq_.size() == rightpq_.size())
            return (leftpq_.top() + rightpq_.top()) / 2.0;
        else return leftpq_.top();
    }
    
private:
    priority_queue<int> leftpq_;
    priority_queue<int, vector<int>, decltype(greater<int>())> rightpq_;
};
