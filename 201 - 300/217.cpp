// Run time 44ms

class Solution {
public:
    bool containsDuplicate(vector<int>& nums) {
        unordered_set<int> numset;
		for (auto i = nums.begin(); i != nums.end(); ++i) {
			auto p = numset.insert(*i);
			if (!p.second) return true;
		}
		return false;
    }
};
