// Run time 9ms

class Solution {
public:
    int maximalSquare(vector<vector<char>>& matrix) {
        int n = matrix.size();
        if (!n) return 0;
        int m = matrix[0].size();
        vector<vector<int>> max_square(n, vector<int>(m));    // max possible square with right-lower corner at (i,j)
        int ans = 0;
        for (int i = 0; i != n; ++i) {
            for (int j = 0; j != m; ++j) {
                if (matrix[i][j] == '0') {
                    max_square[i][j] = 0;
                    continue;
                }
                if (i == 0 || j == 0) max_square[i][j] = 1;
                else max_square[i][j] = min(max_square[i-1][j], min(max_square[i][j-1], max_square[i-1][j-1])) + 1;
                ans = max(ans, max_square[i][j]);   
            }
        }
        
        return ans * ans;
    }
};
