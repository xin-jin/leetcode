// Run time 22ms
// A bit ugly code I suppose :(

class Solution {
public:
    bool isPalindrome(ListNode* head) {
        int n = 0;
        ListNode *cur = head, *tail = head;
        while (cur) {
            ++n;
            tail = cur;
            cur = cur->next;
        }
        if (n < 2) return true;
        ListNode *cur2 = head;
        for (int i = 0; i != (n+1)/2; ++i, cur2 = cur2->next);
        reverse(cur2);
        cur = head;
        cur2 = tail;
        for (int i = 0; i != n/2; ++i) {
            if (cur->val != cur2->val) return false;
            cur = cur->next;
            cur2 = cur2->next;
        }   
        
        return true;
    }
    
private:
    void reverse(ListNode* head) {
        ListNode dummy(0), *cur = head, *pre = &dummy;
        dummy.next = head;
        while (cur) {
            ListNode *tmp = cur->next;
            cur->next = pre;
            pre = cur;
            cur = tmp;
        }
        head->next = nullptr;
    }
};
