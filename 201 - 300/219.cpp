// Run time 28ms

class Solution {
public:
    bool containsNearbyDuplicate(vector<int>& nums, int k) {
		unordered_set<int> numset;
		for (int i = 0; i <= k && i < nums.size(); ++i) {
			auto p = numset.insert(nums[i]);
			if (!p.second) return true;
		}
		for (int i = k+1; i < nums.size(); ++i) {
			numset.erase(nums[i-k-1]);
			auto p = numset.insert(nums[i]);
			if (!p.second) return true;
		}
		return false;
    }
};
