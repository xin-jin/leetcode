// Run time 83ms
// There exists a better solution with KMP algorithm

class Solution {
public:
    string shortestPalindrome(string s) {
		int j = s.size(), n = s.size();
		if (n <= 1) return s;
		string rs = s;
		reverse(rs.begin(), rs.end());
		while (s.substr(0, j) != rs.substr(n-j)) --j;
		return rs + s.substr(j);
    }
};
