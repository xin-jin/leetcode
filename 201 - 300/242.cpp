// Run time 12ms

class Solution {
public:
    bool isAnagram(string s, string t) {
		vector<int> cnt(26, 0);
		for (char& c : s)
			++cnt[c-97];
		for (char& c : t) 
			if (--cnt[c-97] < 0) return false;

		for (int& num : cnt)
			if (num) return false;

		return true;
    }
};
