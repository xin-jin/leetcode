// Run time 0ms

class Solution {
public:
    vector<string> summaryRanges(vector<int>& nums) {
        if (nums.size() == 0) return {};
        int prev = nums[0], low = prev, high = prev;
        vector<string> res;
        nums.push_back(prev-1);
        for (int i = 1; i < nums.size(); ++i) {
            if (nums[i] == prev + 1) {
                ++high;
                ++prev;
            }
            else {
                if (high == low) {
                    res.push_back(to_string(low));
                }
                else {
                    ostringstream ss;
                    ss << low << "->" << high;
                    res.push_back(ss.str());
                }
                low = nums[i];
                high = nums[i];
                prev = low;
            }
        }
        return res;
    }
};
