// Run time 24ms

class Solution {	
public:
	using AdjType = vector<vector<int>>;
    vector<int> findOrder(int numCourses, vector<pair<int, int>>& prerequisites) {
		adj_mat.resize(numCourses);
		visited.resize(numCourses);
        for (auto& edge : prerequisites) {
            adj_mat[edge.first].push_back(edge.second);
        }
        vector<bool> tmp_visited(numCourses, false);
        for (int i = 0; i != numCourses; ++i) {
			if (!dfs(i, tmp_visited)) return vector<int>{};
        }

		return schedule;
    }
	
private:
	AdjType adj_mat;
	vector<int> visited;
	vector<int> schedule;

	bool dfs(int node, vector<bool>& tmp_visited) {
		if (visited[node]) return true;
		visited[node] = true;
		tmp_visited[node] = true;
		for (auto& n : adj_mat[node]) {
			if (tmp_visited[n]) return false;
			if (!dfs(n, tmp_visited)) return false;
		}
		tmp_visited[node] = false;
		schedule.push_back(node);
		return true;
	}
};
