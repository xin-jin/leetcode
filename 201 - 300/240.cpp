// Run time 192ms

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        int i = 0, j = matrix[0].size() - 1;
        while (true) {
            if (i == matrix.size() || j < 0) return false;
            if (matrix[i][j] == target) return true;
            if (matrix[i][j] < target) ++i;
            else --j;
        }
    }
};
