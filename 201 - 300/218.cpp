// Run time 26ms

class Solution {
public:
    vector<pair<int, int>> getSkyline(vector<vector<int>>& buildings) {
		sort(buildings.begin(), buildings.end(), [](vector<int>& x, vector<int>& y) {
				return x[0] < y[0];
			});
		auto cmp = [&buildings](int i, int j) {
			return buildings[i][2] < buildings[j][2];
		};
        priority_queue<int, vector<int>, decltype(cmp)> height(cmp);

		// construct critical points
		vector<int> points;
		for (auto b : buildings) {
			points.push_back(b[0]);
			points.push_back(b[1]);
		}
		sort(points.begin(), points.end());
		
		int bid = 0;
		int current_height = -1;
		vector<pair<int, int>> ret;
		for (int p : points) {
			// pop if top element is outdated
			while (!height.empty() && buildings[height.top()][1] <= p) {
				height.pop();
			}
			// insert new height
			while (bid < buildings.size() && buildings[bid][0] <= p) {
				height.push(bid);
				++bid;
			}
			int top_height = 0;
			if (!height.empty()) {
				top_height = buildings[height.top()][2];
			}
			if (top_height != current_height) {
				current_height = top_height;
				ret.push_back({p, current_height});
			}
		}
		return ret;
    }
};
