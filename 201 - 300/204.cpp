// Run time 308ms

class Solution {
public:
    int countPrimes(int n) {
        if (n <= 2) return 0;
        int sqn = floor(sqrt(n));
        vector<bool> is_prime(n, true);
		int ans;
        for (int i = 2; i <= sqn; ++i) {
            if (is_prime[i]) {
				++ans;
				// any number of the form i * k with k < i is already
				// eliminated in previous rounds
                int j = i * i;	
                while (j <= n) {
                    is_prime[j] = false;
                    j += i;
                }
            }
        }
		for (int i = sqn + 1; i < n; ++i)
			if (is_prime[i]) ++ans;
        return ans;
    }
};
