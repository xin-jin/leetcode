// Run time 3ms

class Solution {
public:
    bool isUgly(int num) {
        if (num <= 0) return false;
        for (int p : {2, 3, 5}) {
            while (num % p == 0) num /= p;
        }
        return num == 1 ? true : false;
    }
};
