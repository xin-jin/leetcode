// Run time 139ms

class Solution {
public:
    int findMaxLength(vector<int>& nums) {
        unordered_map<int, int> diff;
        int res = 0, running_diff = 0;
        diff[0] = -1;
        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] == 1) ++running_diff;
            else --running_diff;
            auto tmp = diff.find(running_diff);
            if (tmp != diff.end()) {
                res = max(res, i - tmp->second);
            }
            else diff[running_diff] = i;
        }
        return res;
    }
};
