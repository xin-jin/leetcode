// Run time 9ms

class Solution {
public:
    vector<int> findMode(TreeNode* root) {
        if (!root) return {};
		unordered_map<int, int> cnt;
		dfs(cnt, root);
		int max_cnt = 0;
		vector<int> rnt;		
		for (auto& e : cnt) {
			if (e.second == max_cnt) rnt.push_back(e.first);
			else if (e.second > max_cnt) {
				max_cnt = e.second;
				rnt = {e.first};
			}
		}

		return rnt;
    }

private:
	void dfs(unordered_map<int, int>& cnt, TreeNode* node) {
		++cnt[node->val];
		if (node->left) dfs(cnt, node->left);
		if (node->right) dfs(cnt, node->right);
	}
};
