// Run Time 0ms

class Solution {
public:
    vector<int> preorderTraversal(TreeNode* root) {
        TreeNode *cptr = root;
		stack<TreeNode*> s;
		vector<int> ans;

		while (cptr || !s.empty()) {
			while (cptr) {
				s.push(cptr);
				ans.push_back(cptr->val);
				cptr = cptr->left;
			}
			cptr = s.top();
			s.pop();
			cptr = cptr->right;
		}

		return ans;
    }
};
