// Run Time 4ms

class Solution {
public:
    int findMin(vector<int>& nums) {
		for (int i = 0, j = nums.size()-1; ; ) {
			if (j-i <= 1) return min(nums[i], nums[j]);
			int mid = (i+j) >> 1;
			if (nums[i] > nums[mid]) j = mid;
			else if (nums[mid] > nums[j]) i = mid;
			else return nums[i];
		}
    }
};
