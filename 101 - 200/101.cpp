// Run Time 4ms
// Recursive

class Solution {
public:
    bool isSymmetric(TreeNode* root) {
		if (!root) return true;
		else return isSymmetric(root->left, root->right);
    }

private:
	bool isSymmetric(TreeNode* l, TreeNode* r) {
		if (l && r && l->val == r->val)
			return isSymmetric(l->left, r->right) && isSymmetric(l->right, r->left);
		else if (!l && !r) return true;
		else return false;
	}
};
