// Run time 43ms
// There exists a more elegant solution: reset the moving pointer to the head
// of the other linked list when it reaches nullptr. See the discussion board
// for details.

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        size_t len1 = 1, len2 = 1;
        ListNode *curA = headA, *curB = headB;
        for (ListNode *cur = headA; cur; cur = cur->next, ++len1);
        for (ListNode *cur = headB; cur; cur = cur->next, ++len2);
        if (len1 > len2)
            for (int i = 0; i != len1 - len2; ++i) curA = curA->next;
        else
            for (int i = 0; i != len2 - len1; ++i) curB = curB->next;
        while (curA != curB) {
            curA = curA->next;
            curB = curB->next;
        }
        
        return curA;
    }
};
