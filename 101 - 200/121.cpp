// Run Time 8ms

class Solution {
public:
    int maxProfit(vector<int>& prices) {
		int _min = INT_MAX, maxProfit = 0;
		for (auto i = prices.begin(); i != prices.end(); ++i) {
			if (*i < _min) _min = *i;
			maxProfit = max(maxProfit, *i-_min);
		}

		return maxProfit;
    }
};
