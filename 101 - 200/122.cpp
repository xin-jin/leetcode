// Run Time 8ms

class Solution {
public:
    int maxProfit(vector<int>& prices) {
		if (!prices.size()) return 0;
		// h is the maxProfit holding one stock, f is the maxProfit with no stock at hand
		int h = -prices[0], f = 0;
		for (auto i = prices.begin()+1; i != prices.end(); ++i) {
			h = max(f-(*i), h);
			f = max(h+(*i), f);
		}

		return f;
    }
};
