// Run Time 20ms

class Solution {
public:
    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
		TreeNode dummy(0), *cnode = &dummy;
		unordered_map<int, TreeNode*> nodeMap {};

		bool turnLeft = false;
		for (auto i = postorder.rbegin(), j = inorder.rbegin(); i != postorder.rend(); ++i) {
			TreeNode *newNode = new TreeNode(*i);
			if (turnLeft) {
				cnode->left = newNode;
				turnLeft = false;
			} else cnode->right = newNode;
			cnode = newNode;
			
			if (*i == *j) {
				turnLeft = true;
				++j;
				auto tmp = nodeMap.find(*j);
				while (tmp != nodeMap.end()) {
					cnode = tmp->second;
					++j;
					tmp = nodeMap.find(*j);
				}
			}
			nodeMap.insert({ *i, newNode });
		}

		return dummy.right;
    }
};
