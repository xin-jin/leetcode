// Run time 6ms

class Solution {
public:
    vector<int> twoSum(vector<int>& numbers, int target) {
		int left = 0, right = numbers.size()-1;
		int cur = numbers[left] + numbers[right];
		while (cur != target) {
			if (cur < target) ++left;
			else --right;
			cur = numbers[left] + numbers[right];
		}

		return {left+1, right+1};
    }
};
