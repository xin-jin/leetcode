// Run time 40ms

class Solution {
private:
    pair<TreeLinkNode*, bool> findNext(TreeLinkNode* node, bool fromLeft) {
        if (!node) return pair<TreeLinkNode*, bool>{nullptr, true};
        if (fromLeft && node->left) return pair<TreeLinkNode*, bool>{node, true};
        if (node->right) return pair<TreeLinkNode*, bool>{node, false};
        return findNext(node->next, true);
    }

public:
    void connect(TreeLinkNode *root) {
        if (!root) return;
        TreeLinkNode *leftmost = root;
        pair<TreeLinkNode*, bool> cur;
        while (true) {
			cur = findNext(leftmost, true);
			if (!cur.first) break;
			if (cur.second) leftmost = cur.first->left;
			else leftmost = cur.first->right;
            while (true) {
                pair<TreeLinkNode*, bool> next;
				if (cur.second) next = findNext(cur.first, false);
				else next = findNext(cur.first->next, true);
                TreeLinkNode *leftnode, *rightnode;
                if (cur.second) leftnode = cur.first->left;
                else leftnode = cur.first->right;
                if (!next.first) {
                    leftnode->next = nullptr;
                    break;
                }
                if (next.second) rightnode = next.first->left;
                else rightnode = next.first->right;
                leftnode->next = rightnode;
                cur = next;
            }
        }
    }
};
