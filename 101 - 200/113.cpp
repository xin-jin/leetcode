// Run Time 12ms

class Solution {
public:
    vector<vector<int>> pathSum(TreeNode* root, int sum) {
        vector<vector<int>> ans;
		vector<int> acc;

		pathSum(ans, acc, root, sum);

		return ans;
    }

private:
	void pathSum(vector<vector<int>>& ans, vector<int>& acc, TreeNode* root, int sum) {
		if (!root) return;
		acc.push_back(root->val);
		if (!root->left && !root->right && sum == root->val) ans.push_back(acc);
		else {
			if (root->left) pathSum(ans, acc, root->left, sum - root->val);
			if (root->right) pathSum(ans, acc, root->right, sum - root->val);
		}
		acc.pop_back();
	}
};
