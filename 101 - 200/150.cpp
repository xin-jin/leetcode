// Run time 12ms

class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        stack<int> nums;
        for (const string& tmp : tokens) {
                if (tmp == "+") {
                    auto e = getNums(nums);
                    nums.push(e.first + e.second);
                }
                else if (tmp == "-") {
                    auto e = getNums(nums);
                    nums.push(e.first - e.second);
                }
                else if (tmp == "*") {
                    auto e = getNums(nums);
                    nums.push(e.first * e.second);
                }
                else if (tmp ==  "/") {
                    auto e = getNums(nums);
                    nums.push(e.first / e.second);
                }
                else nums.push(stoi(tmp));
        }
        
        return nums.top();
    }
    
private:
    pair<int, int> getNums(stack<int>& nums) {
        int e2 = nums.top();
        nums.pop();
        int e1 = nums.top();
        nums.pop();
        return { e1, e2 };
    }
};
