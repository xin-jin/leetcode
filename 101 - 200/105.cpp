// Run Time 20ms

class Solution {
public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
		TreeNode dummy(0), *cnode =&dummy;
		unordered_map<int, TreeNode*> nodeMap {};

		auto j = inorder.begin();
		bool turnRight = false;
		for (auto i = preorder.begin(); i != preorder.end(); ++i) {
			TreeNode *newNode = new TreeNode(*i);
			if (turnRight) {
				cnode->right = newNode;
				turnRight = false;
			} else {
				cnode->left = newNode;
			}
			cnode = newNode;
			nodeMap.insert({*i, newNode});
			
			if (*i == *j) {
				turnRight = true;
				++j;
				auto tmp = nodeMap.find(*j);
				while (tmp != nodeMap.end()) {
					cnode = tmp->second;
					++j;
					tmp = nodeMap.find(*j);
				}
			}
		}
		
		return dummy.left;
    }
};
