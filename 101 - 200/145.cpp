// Run Time 0ms

class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> ans;
		TreeNode *cptr = root, *lastAdd;
		stack<TreeNode*> s;

		while (cptr || !s.empty()) {
			while (cptr) {
				s.push(cptr);
				cptr = cptr->left;
			}
			cptr = s.top()->right;
			if (!cptr || cptr == lastAdd) {
				ans.push_back(s.top()->val);
				lastAdd = s.top();
				s.pop();
				cptr = NULL;
			}
		}

		return ans;
    }
};
