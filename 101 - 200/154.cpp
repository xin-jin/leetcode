// Run Time 8ms

class Solution {
public:
    int findMin(vector<int>& nums) {
		return findMin(nums, 0, nums.size()-1);
    }

private:
	int findMin(const vector<int>& nums, int i, int j) {
		if (j - i <= 1) return min(nums[i], nums[j]);
		if (nums[i] < nums[j]) return nums[i];
		return min(findMin(nums, i, (i+j)/2), findMin(nums, (i+j)/2+1, j));
	}
};
