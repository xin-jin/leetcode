// Run Time 20ms

class Solution {
public:
    TreeNode* sortedArrayToBST(vector<int>& nums) {
		if (!nums.size()) return NULL;
		TreeNode *root = new TreeNode(0);
		sortedArrayToBST(nums, 0, nums.size()-1, root);

		return root;
    }

private:
	void sortedArrayToBST(vector<int>& nums, int s, int t, TreeNode* newNode) {
		int mid = (s+t)/2;
		newNode->val = nums[mid];

		if (s != t) {
			if (s != mid) {
				TreeNode *left = new TreeNode(0);
				newNode->left = left;
				sortedArrayToBST(nums, s, mid-1, left);
			}			
			TreeNode *right = new TreeNode(0);
			newNode->right = right;
			sortedArrayToBST(nums, mid+1, t, right);
		}
	}
};
