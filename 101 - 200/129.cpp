// Run time 4ms

class Solution {
public:
    int sumNumbers(TreeNode* root) {
		if (!root) return 0;
		dfs(root, 0);
		return ans;
    }

private:
	void dfs(TreeNode* node, int path_sum) {
		path_sum = path_sum*10 + node->val;
		if (node->left) dfs(node->left, path_sum);
		if (node->right) dfs(node->right, path_sum);
		if (!node->left && !node->right) ans += path_sum;
	}
	int ans = 0;
};
