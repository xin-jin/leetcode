// Run time 12ms

class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
		if (!head) return NULL;
		ListNode *slow_ptr = head, *fast_ptr = head;
		while (true) {
			slow_ptr = slow_ptr->next;
			if (!fast_ptr || !(fast_ptr->next)) return NULL;
			fast_ptr = fast_ptr->next->next;
			if (slow_ptr == fast_ptr) break;
		}

		ListNode *another_ptr = head;
		while (head != slow_ptr) {
			head = head->next;
			slow_ptr = slow_ptr->next;
		}		
		
		return slow_ptr;
    }
};
