// Run time 19ms

class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        if (!k) return;
        int n = nums.size(), count = 0;
        for (int i = 0; ; ++i) {
            int cur = i, prev = nums[i];
            do {
                cur = (cur + k) % n;
                swap(prev, nums[cur]);
                if ((++count) == n) return;
            } while (cur != i);
        }
    }
};
