// Run time 4ms

class Solution {
public:
    int trailingZeroes(int n) {
		unsigned divisor = 5, cnt = 0;
		while (true) {
			unsigned added = n / divisor;
			cnt += n / divisor;
			if (added < 5) break;
			divisor *= 5;
		}

		return cnt;
    }
};
