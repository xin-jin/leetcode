// Run time 12ms

class Solution {
public:
    vector<vector<string>> partition(string s) {
        vector<vector<string>> pars;
        vector<string> acc;
        partition_(pars, acc, 0, s.size(), s);
        return pars;
    }

private:
    void partition_(vector<vector<string>>& pars, vector<string>& acc, int head, int size, const string& s) {
        if (!size) {
            pars.push_back(acc);
            return;
        }
        string tmp = "";
        for (int i = 1; i <= size; ++i) {
            tmp.push_back(s[head+i-1]);
            if (isPalindrome(head, i, s)) {
                acc.push_back(tmp);
                partition_(pars, acc, head+i, size-i, s);
                acc.pop_back();
            }
        }
    }

    bool isPalindrome(int head, int size, const string& s) {
        int i = head, j = head + size - 1;
        while (i < j) {
            if (s[i++] != s[j--]) return false;
        }
        return true;
    }
};
