// Run Time 28ms

class Solution {
public:
    TreeNode* sortedListToBST(ListNode* head) {
		if (!head) return NULL;
		auto *root = new TreeNode(head->val);
        int size = 1;
		ListNode *tail = head;
		for ( ; tail->next; tail = tail->next) ++size;
		sortedListToBST(head, size, root);

		return root;
    }

private:
	void sortedListToBST(ListNode* head, int size, TreeNode* newNode) {
		ListNode *mid = head;
		for (int i = 1; i < (size+1)/2; ++i) mid = mid->next;
		newNode->val = mid->val;
		
		if (size > 1) {
			if (size > 2) {
				auto left = new TreeNode(0);
				newNode->left = left;
				sortedListToBST(head, (size-1)/2, left);
			}
			auto right = new TreeNode(0);
			newNode->right = right;
			sortedListToBST(mid->next, size-(size+1)/2, right);
		}
	}
};
