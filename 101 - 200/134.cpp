// Run time 8ms

class Solution {
public:
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
		int ans = 0, accum = 0, sum = 0;
		for (int i = 0; i != gas.size(); ++i) {
			int net_cost = gas[i] - cost[i];
			sum += net_cost;
			accum += net_cost;
			if (accum < 0) {
				ans = i+1;
				accum = 0;
			}
		}
		if (sum < 0) return -1;
		else return ans;
    }
};
