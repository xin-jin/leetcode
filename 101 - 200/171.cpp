// Run time 8ms

class Solution {
public:
    int titleToNumber(string s) {
		int weight = 1, col = 0;
		for (int i = s.size() - 1; i != -1; --i) {
			col += (s[i] - 64) * weight;
			weight *= 26;
		}

		return col;
    }
};
