// Run time 16ms

class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        k = nums.size() - k % nums.size();
        if (k == nums.size()) return;
        int left = 0, right = k, first = 0, second = k;
        while (left != right) {
            swap(nums[left++], nums[right++]);
            if (right == nums.size()) right = second;
            if (left == second) second = right;
        }
    }
};
