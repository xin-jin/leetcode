// Run time 16ms

class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int count = 0, cur;
        for (int x : nums) {
            if (count == 0) {
                count = 1;
                cur = x;
            }
            else if (cur == x) {
                ++count;
            }
            else --count;
        }
        return cur;
    }
};
