// Run Time 28ms

class Solution {
public:
    ListNode* insertionSortList(ListNode* head) {
		if (!head) return head;
		ListNode dummy(0), *cptr = head;
		dummy.next = head;

		while (cptr->next) {
			if (cptr->next->val < cptr->val) {
				int val = cptr->next->val;							
				ListNode *inPos = &dummy, *tmp = cptr->next->next;;
				for ( ; inPos->next->val < val; inPos = inPos->next);
				cptr->next->next = inPos->next;
				inPos->next = cptr->next;
				cptr->next = tmp;
			}
			else cptr = cptr->next;
		}

		return dummy.next;
    }
};
