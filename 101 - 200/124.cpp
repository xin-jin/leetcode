// Run time 33ms

class Solution {
public:
    int maxPathSum(TreeNode* root) {
		return helper(root).second;
    }

	pair<int, int> helper(TreeNode* n) {
		// @return first int is the max sum ending at n, and
		// second int is the max sun in the subtree rooted at n
		if (!n) return {INT_MIN, INT_MIN};
		auto lp = helper(n->left), rp = helper(n->right);
		return {max(0, max(lp.first, rp.first))+n->val,
				max(max(lp.second, rp.second),
					max(0, lp.first) + n->val + max(0, rp.first))};
	}
};
