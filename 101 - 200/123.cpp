// Run Time 8ms

class Solution {
public:
    int maxProfit(vector<int>& prices) {
        if (prices.size() <= 1) return 0;
		// hij is the current maxProfit when i transactions have been done and j stocks are at hand
		int h01 = -min(prices[0], prices[1]),
			h10 = prices[1]-prices[0],
			h11 = INT_MIN, h20 = INT_MIN;

		for (auto i = prices.begin()+2; i != prices.end(); ++i) {
			h01 = max(h01, -(*i));
			h10 = max(h10, h01+(*i));
			h11 = max(h11, h10-(*i));
			h20 = max(h20, h11+(*i));
		}

		return max(max(0, h10), h20);
    }
};
