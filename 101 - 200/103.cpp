// Run Time 4ms

class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        vector<vector<int>> ans;

        if (root) {
            ans.push_back(vector<int>({root->val}));
			auto lastLevel = vector<TreeNode*> {root};
			bool fromRight = true;

            while (true) {
				vector<TreeNode*> newLevel {};
				for (auto i = lastLevel.rbegin(); i != lastLevel.rend(); ++i) {
					if (fromRight) {
						if ((*i)->right) newLevel.push_back((*i)->right);
						if ((*i)->left) newLevel.push_back((*i)->left);
					} else {
						if ((*i)->left) newLevel.push_back((*i)->left);
						if ((*i)->right) newLevel.push_back((*i)->right);
					}
				}
				
				if (newLevel.empty()) break;
				ans.push_back(vector<int>());
				for (auto i = newLevel.begin(); i != newLevel.end(); ++i) {
					ans.back().push_back((*i)->val);
				}
				fromRight = !fromRight;
				lastLevel = newLevel;
			}
		}

		return ans;
	}
};
