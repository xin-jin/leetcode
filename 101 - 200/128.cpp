// Run time 23ms

class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
		unordered_set<int> nset;
		for (int n : nums) nset.insert(n);
		int max_len = 0;
		for (int n : nums) {
			if (nset.find(n-1) == nset.end()) {				
				int len = 1, m = n+1;
				while (nset.find(m) != nset.end()) {
					++len;
					--m;
				}
				max_len = max(max_len, len);
			}
		}
        
        return max_len;
    }
};
