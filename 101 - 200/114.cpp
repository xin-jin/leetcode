// Run Time 8ms

class Solution {
public:
    void flatten(TreeNode* root) {
		TreeNode dummy(0), *cnode = &dummy;
		list<TreeNode*> preorder;
		getPreorder(preorder, root);
		for (auto i = preorder.begin(); i != preorder.end(); ++i) {
			cnode->right = *i;
			cnode = *i;
			cnode->left = nullptr;
			cnode->right = nullptr;
		}
    }

private:
	void getPreorder(list<TreeNode*>& preorder, TreeNode* cnode) {
		if (cnode) {
			preorder.push_back(cnode);
			getPreorder(preorder, cnode->left);
			getPreorder(preorder, cnode->right);
		}
	}
};
