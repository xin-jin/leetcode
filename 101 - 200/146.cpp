// Run time 76ms

class LRUCache{
public:
    LRUCache(int capacity): capacity_(capacity) {

    }

    int get(int key) {
        auto tmp = map_.find(key);
        if (tmp != map_.end()) {
			moveFront_(tmp->first);
            return tmp->second.first;
        }
        else return -1;
    }

    void set(int key, int value) {
        auto tmp = map_.find(key);
        if (tmp != map_.end()) {
			moveFront_(tmp->first);
            tmp->second.first = value;
        }
        else {
            if (list_.size() >= capacity_) {
                map_.erase(list_.back());
                list_.pop_back();
            }
            list_.push_front(key);
            map_.insert({ key, { value, list_.begin() }});
        }
    }

private:
    int capacity_;
    list<int> list_;
    unordered_map<int, pair<int, list<int>::iterator>> map_;

    void moveFront_(int key) {
        auto tmp = map_.find(key);
        list_.erase(tmp->second.second);
        list_.push_front(key);
        tmp->second.second = list_.begin();
    }
};
