// Run time 6ms

class Solution {
public:
    int numIslands(vector<vector<char>>& grid) {
		_nrows = grid.size();
		if (!_nrows) return 0;
		_ncols = grid[0].size();
		for (int i = 0; i != _nrows; ++i) {
			for (int j = 0; j != _ncols; ++j) {
			    if (grid[i][j] == '1') {
			        ++_count;
				    dfs(grid, i, j);
			    }
			}
		}
		return _count;
    }

private:
	int _count = 0, _nrows, _ncols;
	void dfs(vector<vector<char>>& grid, int i, int j) {
		if (grid[i][j] == '0') return;
		grid[i][j] = '0';
		if (i < _nrows - 1) dfs(grid, i+1, j);
		if (j < _ncols - 1) dfs(grid, i, j+1);
		if (j > 0) dfs(grid, i, j-1);
		if (i > 0) dfs(grid, i-1, j);
	}
};
