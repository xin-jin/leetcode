// Run Time 0ms

class Solution {
public:
    vector<vector<int>> generate(int numRows) {
		vector<vector<int>> ans;

		if (numRows < 1) return ans;
		ans.push_back(vector<int>({1}));

		for (int i = 2; i <= numRows; ++i) {
			auto lastRow = ans.back();
			vector<int> newRow {1};

			for (int j = 0; j < lastRow.size() - 1; ++j) {
				newRow.push_back(lastRow[j] + lastRow[j+1]);
			}

			newRow.push_back(1);
			ans.push_back(newRow);
		}

		return ans;
    }
};
