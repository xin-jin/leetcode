// Run Time 8ms

class Solution {
public:
    vector<vector<int>> levelOrderBottom(TreeNode* root) {
		vector<vector<int>> ans;
		int maxLevel = -1;
		
		levelOrder(ans, 0, maxLevel, root);
		reverse(ans.begin(), ans.end());

		return ans;
    }

private:
	void levelOrder(vector<vector<int>>& ans, int level, int& maxLevel, TreeNode* root) {
		if (!root) return;
		if (level > maxLevel) {
			maxLevel = level;
			ans.push_back(vector<int> {});
		}
		ans[level].push_back(root->val);
		levelOrder(ans, level+1, maxLevel, root->left);
		levelOrder(ans, level+1, maxLevel, root->right);
	}
};
