// Run time 6ms

class Solution {
public:
    string largestNumber(vector<int>& nums) {
        vector<string> numstrs;
        for (int x : nums) {
            numstrs.push_back(to_string(x));
        }
        sort(numstrs.begin(), numstrs.end(), [](const string& s1, const string& s2) { return s1 + s2 > s2 + s1; });
        if (numstrs[0] == "0") return "0";
        string ans;
        for (const string& s : numstrs) ans += s;
		
        return ans;
    }
};
