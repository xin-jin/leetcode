// Run time 6ms

class Solution {
public:
    int findPeakElement(vector<int>& nums) {
        if (nums.size() == 1) return 0;
        if (nums[0] > nums[1]) return 0;
        if (nums[nums.size() - 1] > nums[nums.size() - 2]) return nums.size() - 1;
        return helper(nums, 0, nums.size()-1);
    }
    
    int helper(vector<int>& nums, int left, int right) {
        int mid = left + (right - left) / 2;
        if (nums[mid] < nums[mid - 1]) return helper(nums, left, mid);
        if (nums[mid] < nums[mid + 1]) return helper(nums, mid, right);
        return mid;
    }
};
