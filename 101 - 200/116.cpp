// Run time 24ms

class Solution {
public:
    void connect(TreeLinkNode *root) {
		if (!root) return;
		TreeLinkNode *leftmost = root;
		while (leftmost->left) {
			TreeLinkNode *cur = leftmost;
			while (cur) {
				cur->left->next = cur->right;
				if (cur->next)
					cur->right->next = cur->next->left;
				cur = cur->next;
			}
			leftmost = leftmost->left;
		}
	}
};
