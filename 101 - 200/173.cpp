// Run time 23ms

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class BSTIterator {
public:
    BSTIterator(TreeNode *root) {
        TreeNode *ptr = root;
        while (ptr) {
            nodes_.push(ptr);
            ptr = ptr->left;
        }
    }

    /** @return whether we have a next smallest number */
    bool hasNext() {
        return !nodes_.empty();
    }

    /** @return the next smallest number */
    int next() {
        TreeNode *cur = nodes_.top();
        nodes_.pop();
        int rnt = cur->val;
        if (cur->right) {
            cur = cur->right;
            while (cur) {
                nodes_.push(cur);
                cur = cur->left;
            }
        }
        return rnt;
    }
    
private:
    stack<TreeNode*> nodes_;
};

/**
 * Your BSTIterator will be called like this:
 * BSTIterator i = BSTIterator(root);
 * while (i.hasNext()) cout << i.next();
 */
