// Run Time 4ms
// Iterative

class Solution {
public:
    bool isSymmetric(TreeNode* root) {
		if (!root) return true;
		stack<TreeNode*> lStack, rStack;

		if (root->left && root->right)
			for (lStack.push(root->left), rStack.push(root->right); !lStack.empty(); ) {
				TreeNode *l = lStack.top(), *r = rStack.top();
				lStack.pop(); rStack.pop();
				if (l->val == r->val) {
					if (l->right && r->left) {
						lStack.push(l->right);
						rStack.push(r->left);
					}
					else if (l->right || r->left) return false;
					if (l->left && r->right) {
						lStack.push(l->left);
						rStack.push(r->right);
					}
					else if (l->left || r->right) return false;
				}
				else return false;
			}
		else if (!root->left && !root->right) return true;
		else return false;

		return true;
    }
};
