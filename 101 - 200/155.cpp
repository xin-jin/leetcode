// Run Time 28ms

class MinStack {
public:
    void push(int x) {
		v.push(element(x, min(getMin(), x)));
    }

    void pop() {
        v.pop();
    }

    int top() {
        return v.top().x;
    }

    int getMin() {
        if (!v.size()) return INT_MAX;
		else return v.top().m;
    }

private:
	struct element {
		int x, m;
		element(int a, int b):x(a), m(b) {}
	};
	stack<element> v;
};
