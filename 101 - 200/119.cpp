// Run Time 0ms

class Solution {
public:
    vector<int> getRow(int rowIndex) {
		vector<int> ans(rowIndex+1);

		ans[0] = ans[1] = 1;
		for (int k = 2; k <= rowIndex; ++k) {
			ans[k] = 1;
			for (int i = k-1; i != 0; --i) {
				ans[i] += ans[i-1];
			}			
		}

		return ans;
    }
};
