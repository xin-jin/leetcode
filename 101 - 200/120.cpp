// Run time 8ms

class Solution {
public:
    int minimumTotal(vector<vector<int>>& triangle) {
        if (!triangle.size()) return 0;

        // stores the min path to a row
        vector<int> min_p(triangle.size());
        min_p[0] = triangle[0][0];
        for (int i = 1; i < triangle.size(); ++i) {
            vector<int> &c_row = triangle[i];
            min_p[i] = min_p[i-1] + c_row[i];

            for (int j = i-1; j > 0; --j) {
                min_p[j] = min(min_p[j-1], min_p[j]) + c_row[j];
            }
            min_p[0] = min_p[0] + c_row[0];
        }

        return *min_element(min_p.cbegin(), min_p.cend());
    }
};
