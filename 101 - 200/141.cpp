// Run time 12ms

class Solution {
public:
    bool hasCycle(ListNode *head) {
		if (!head) return false;
		ListNode *slow_ptr = head, *fast_ptr = head;
		while (true) {
			slow_ptr = slow_ptr->next;
			if (!fast_ptr || !(fast_ptr->next)) break;
			fast_ptr = fast_ptr->next->next;
			if (slow_ptr == fast_ptr) return true;
		}
		return false;
    }
};
