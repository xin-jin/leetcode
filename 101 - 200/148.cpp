// Run Time 60ms

class Solution {
public:
    ListNode* sortList(ListNode* head) {
		if (!head) return head;
		int size = 1;
		ListNode *tail = head;

		for ( ; tail->next; ++size) tail = tail->next;

		return sortList(head, tail, size);
    }

private:
	ListNode* sortList(ListNode* head, ListNode* tail, int size) {
		if (head == tail) return head;
		ListNode dummy(0), *midtail = &dummy;
		dummy.next = head;
		int mid = size/2;
		
		midtail = head;
		for (int j = 1 ; j != mid; ++j) midtail = midtail->next;
		ListNode *secondHead = midtail->next;
		midtail->next = NULL;
		ListNode *n1 = sortList(head, midtail, mid), *n2 = sortList(secondHead, tail, size-mid);
		ListNode *i = &dummy;
		int s1 = 0, s2 = mid;
		while (s1 != mid && s2 != size) {
			if (n1->val < n2->val) {
				i->next = n1;
				n1 = n1->next;
				++s1;
			}
			else {
				i->next = n2;
				n2 = n2->next;
				++s2;
			}
			i = i->next;
		}
		if (s2 == size) i->next = n1;
		else i->next = n2;

		return dummy.next;
	}
};
