// Run time 6ms
// There exists more element way to write it

class Solution {
public:
    int maxProduct(vector<int>& nums) {
        bool cont = false;
        int pos_prod = 0, neg_prod = 0, max_prod = INT_MIN;
        for (int i = 0; i != nums.size(); ++i) {
			max_prod = max(max_prod, nums[i]);
            if (nums[i] > 0) {
                if (pos_prod) pos_prod *= nums[i];
                else pos_prod = nums[i];
                if (neg_prod) neg_prod *= nums[i];
                max_prod = max(max_prod, pos_prod);
            }
            else if (nums[i] < 0) {
				int tmp = neg_prod;
                if (pos_prod) {
                    neg_prod = pos_prod * nums[i];
                }
				else neg_prod = nums[i];
                if (tmp) {
                    pos_prod = tmp * nums[i];
					max_prod = max(max_prod, pos_prod);
                }
				else pos_prod = 0;
            }
            else {
                pos_prod = neg_prod = 0;
            }
        }
        return max_prod;
    }
};
