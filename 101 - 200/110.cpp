// Run Time 16ms

class Solution {
public:
    bool isBalanced(TreeNode* root) {
		if (BalancedHeight(root) == -1) return false;
		else return true;
    }

private:
	int BalancedHeight(TreeNode* root) {
		if (!root) return 0;
		auto lpair = BalancedHeight(root->left),
			rpair = BalancedHeight(root->right);
		if ((lpair == -1 || rpair == -1) || (abs(lpair-rpair) > 1)) return -1;
		
		return max(lpair, rpair)+1;
	}
};
