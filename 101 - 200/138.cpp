// Run time 109ms

/**
 * Definition for singly-linked list with a random pointer.
 * struct RandomListNode {
 *     int label;
 *     RandomListNode *next, *random;
 *     RandomListNode(int x) : label(x), next(NULL), random(NULL) {}
 * };
 */
class Solution {
public:
    RandomListNode *copyRandomList(RandomListNode *head) {
        unordered_map<RandomListNode*, RandomListNode*> pmap;
        RandomListNode *cur = head, dummy(0), *pre = &dummy;
        while (cur) {
            auto pmap_result = pmap.find(cur);
            RandomListNode *n;
            if (pmap_result == pmap.end()) {
                n = new RandomListNode(cur->label);
                pmap.insert({ cur, n });
            }
            else {
                n = pmap_result->second;
            }
            pre->next = n;
            pre = n;
            if (cur->random) {
                pmap_result = pmap.find(cur->random);
                RandomListNode *rn;
                if (pmap_result == pmap.end()) {
                    rn = new RandomListNode(cur->random->label);
                    pmap.insert({ cur->random, rn});
                }
                else {
                    rn = pmap_result->second;
                }
                n->random = rn;
            }
            else {
                n->random = nullptr;
            }
            cur = cur->next;
        }

        return dummy.next;
    }
};
