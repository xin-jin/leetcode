// Run time 69ms

/**
 * Definition for undirected graph.
 * struct UndirectedGraphNode {
 *     int label;
 *     vector<UndirectedGraphNode *> neighbors;
 *     UndirectedGraphNode(int x) : label(x) {};
 * };
 */
class Solution {
public:
    using ugn = UndirectedGraphNode;
    UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
        if (!node) return nullptr;
        unordered_map<ugn*, ugn*> umap;
        return constructNode(node, umap);
    }
    
private:
    ugn* constructNode(ugn* node, unordered_map<ugn*, ugn*>& umap) {
        auto tmp = umap.find(node);
        if (tmp == umap.end()) {
            ugn *new_node = new ugn(node->label);
            umap.insert({ node, new_node });    
            for (ugn* n : node->neighbors) {
                ugn *get_node = constructNode(n, umap);
                new_node->neighbors.push_back(get_node);
            }
            return new_node;
        }
        else return tmp->second;
    }
};
