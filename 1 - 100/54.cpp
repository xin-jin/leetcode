// Run Time 0ms

class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
		if (!matrix.size()) return {};
		vector<int> ans, bi {0, matrix.size()-1}, bj {0, matrix[0].size()-1};

		outputSpiral(matrix, ans, bi, bj);

		return ans;
    }

private:
	void outputSpiral(vector<vector<int>>& matrix, vector<int>& ans, vector<int>& bi, vector<int>& bj) {
		int i = bi[0], j = bj[0];

		if (bi[0] > bi[1] || bj[0] > bj[1]) return;
		if (bi[0] == bi[1]) { for ( ; j <= bj[1]; ++j) ans.push_back(matrix[i][j]); return; }
		if (bj[0] == bj[1]) { for ( ; i <= bi[1]; ++i) ans.push_back(matrix[i][j]); return; }
		
		for ( ; j < bj[1]; ++j) ans.push_back(matrix[i][j]);
		for (i = bi[0], j = bj[1]; i < bi[1]; ++i) ans.push_back(matrix[i][j]);
		for (i = bi[1], j = bj[1]; j > bi[0]; --j) ans.push_back(matrix[i][j]);
		for (i = bi[1], j = bj[0]; i > bi[0]; --i) ans.push_back(matrix[i][j]);

		bi = {bi[0]+1, bi[1]-1}; bj = {bj[0]+1, bj[1]-1};

		outputSpiral(matrix, ans, bi, bj);
	}
};
