// Run Time 8ms

class Solution {
public:
    ListNode* partition(ListNode* head, int x) {
		ListNode dummy(0), *cptr = &dummy, *boundary = &dummy, *prev = &dummy;
		dummy.next = head;

		while (boundary->next && boundary->next->val < x) boundary = boundary->next;
		for (cptr = boundary->next, prev = boundary; cptr; prev = cptr, cptr = cptr->next)
			if (cptr->val < x) {
				prev->next = cptr->next;
				cptr->next = boundary->next;
				boundary->next = cptr;
				boundary = cptr;
				cptr = prev;
			}

		return dummy.next;
    }
};
