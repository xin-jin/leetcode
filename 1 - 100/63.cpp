// Run Time 3ms

class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        int m = obstacleGrid.size();
        if (m == 0) return 0;
        int n = obstacleGrid[0].size();
        vector<int> count(n, 0);
        count[0] = 1;
        for (int i = 0; i < m; ++i) {
            if (obstacleGrid[i][0] == 1)
                count[0] = 0;
            for (int j = 1; j < n; ++j) {
                if (obstacleGrid[i][j] == 1) count[j] = 0;
                else count[j] += count[j-1];
            }
        }
        return count[n-1];
    }
};
