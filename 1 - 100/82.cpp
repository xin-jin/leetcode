// Run Time 8ms

class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
		ListNode tmp(0);
		ListNode *dummy = &tmp, *prev = dummy;

		dummy->next = head;
		for (auto cptr = head; cptr; )
			if (cptr->next && cptr->next->val == cptr->val) {
				auto j = cptr->next;
				while (j && j->val == cptr->val) j = j->next;
				prev->next = j;
				cptr = j;
			}
			else { prev = cptr; cptr = cptr->next; }

		return dummy->next;
    }
};
