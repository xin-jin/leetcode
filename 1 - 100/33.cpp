// Run Time 4ms

class Solution {
public:
    int search(vector<int>& nums, int target) {
		return search(nums, target, 0, nums.size()-1, false);
    }

private:
	int search(vector<int>& nums, int target, int i, int j, bool sorted) {
		const int mid = (i+j)/2;

		if (j-i <= 1) {
			if (nums[i] == target) return i;
			if (nums[j] == target) return j;
			return -1;
		}

		if (target == nums[mid]) return mid;		
		if (sorted) {
			if (target > nums[j] || target < nums[i]) return -1;
			if (target < nums[mid]) return search(nums, target, i, mid-1, true);
			else return search(nums, target, mid+1, j, true);
		}
		else {
			int lreturn;
			if (nums[i] < nums[mid-1]) lreturn = search(nums, target, i, mid-1, true);
			else lreturn = search(nums, target, i, mid-1, false);
			if (lreturn != -1) return lreturn;
			if (nums[mid+1] < nums[j]) return search(nums, target, mid+1, j, true);
			else return search(nums, target, mid+1, j, false);			
		}
	}
};
