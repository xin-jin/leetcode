// Run time 13ms

class Solution {
public:
    bool canJump(vector<int>& nums) {
        int last_possible = nums.size()-1;
        for (int j = nums.size()-2; j >= 0; --j) {
            if (nums[j] + j >= last_possible) last_possible = j;
        }
        return last_possible == 0 ? true : false;
    }
};
