// Run Time 4ms

class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {
		vector<vector<int>> ans {{}};
		vector<int> acc;

		genSubsets(ans, nums, acc, 0);

		return ans;
    }

private:
	void genSubsets(vector<vector<int>>& ans, vector<int>& nums, vector<int>& acc, int start) {
		if (start >= nums.size()) return;
		for (int i = start; i < nums.size(); ++i) {
			acc.push_back(nums[i]);
			ans.push_back(acc);
			genSubsets(ans, nums, acc, i+1);
			acc.erase(acc.end()-1);
		}
	}
};
