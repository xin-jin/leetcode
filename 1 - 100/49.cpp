// Run Time 44ms

class Solution {
public:
    vector<string> anagrams(vector<string>& strs) {
		unordered_map<string, pair<bool, int>> sdict;
		vector<string> ans;

		for (int i = 0; i != strs.size(); ++i) {
			string sorted_str = strs[i];
			sort(sorted_str.begin(), sorted_str.end());

			auto find_ptr = sdict.find(sorted_str);
			if (find_ptr != sdict.end()) {
				ans.push_back(strs[i]);
				find_ptr->second.first = true;
			}
			else sdict.insert(make_pair(sorted_str, make_pair(false, i)));
		}

		for (auto i = sdict.begin(); i != sdict.end(); ++i)
			if (i->second.first) ans.push_back(strs[i->second.second]);

		return ans;
    }
};
