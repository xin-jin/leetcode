// Run Time 4ms

class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
		int len = strs.size(), len1;
		char ch;
		string ans;
		
		if (len == 0) {return "";}
		len1 = strs[0].size();
		if (len == 1) {return strs[0];}
		for (auto i = 0; i < len1; ++i) {
			ch = strs[0][i];
			for (auto j = 1; j < len; ++j) {
				if (strs[j][i] != ch) {return ans;}
			}
			ans += ch;
		}

		return ans;
    }
};
 
