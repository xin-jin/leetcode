// Run Time 12ms

class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
		if (nums.back() < target) return vector<int>{-1, -1};
		if (nums.front() > target) return vector<int>{-1, -1};
		
		return vector<int>{searchLeft(nums, 0, nums.size()-1, target), searchRight(nums, 0, nums.size()-1, target)};
    }

private:
	int searchRight(vector<int>& nums, int i, int j, int target) {
		if (nums[j] == target) return j;
		if (nums[j] < target) return -1;
		int mid = (i+j)/2+1;
		if (target >= nums[mid]) return searchRight(nums, mid, j, target);
		else return searchRight(nums, i, mid-1, target);
	}

	int searchLeft(vector<int>& nums, int i, int j, int target) {
		if (nums[i] == target) return i;
		if (nums[i] > target) return -1;
		int mid = (i+j)/2;
		if (target <= nums[mid]) return searchLeft(nums, i, mid, target);
		else return searchLeft(nums, mid+1, j, target);
	}
};
