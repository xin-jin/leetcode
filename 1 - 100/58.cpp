// Run Time 4ms

class Solution {
public:
    int lengthOfLastWord(string s) {
		if (s == "") return 0;
		auto i = s.crbegin();
		while (*i == ' ' && i != s.crend()) ++i;
		if (i == s.crend()) return 0;
		auto j = i;
		while (*i != ' ' && i != s.crend()) ++i;

		return i - j;
    }
};
