// Run Time 24ms

class Solution {
public:
    ListNode* reverseKGroup(ListNode* head, int k) {
		if (k == 1 || !head) return head;
		ListNode dummy(0), *prev, *cptr = head, *nptr, *lastTail = &dummy;
		dummy.next = head;

		while (true) {
			for (int i = 0; i != k; ++i)
				if (cptr) cptr = cptr->next;
				else return dummy.next;

			prev = head;
			cptr = head->next;
			for (int i = 1; i != k; ++i) {
				nptr = cptr->next;
				cptr->next = prev;
				prev = cptr;
				cptr = nptr;
			}
			lastTail->next = prev;
			lastTail = head;
			head->next = cptr;
			head = cptr;
		}

		return dummy.next;
    }
};
