#!/usr/bin/env python2

class Solution(object):
    def twoSum(self, nums, target):
        nums_dict = {}
        for i in range(len(nums)):
            if nums[i] in nums_dict:
                return [i, nums_dict[nums[i]]]
            else:
                nums_dict[target - nums[i]] = i
