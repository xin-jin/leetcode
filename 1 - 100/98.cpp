// Run Time 16ms

class Solution {
public:
    bool isValidBST(TreeNode* root) {
		if (!root) return true;
		stack<TreeNode*> s;
		long long m = static_cast<long long>(INT_MIN) - 1;
		TreeNode *cptr = root;

		while (cptr || !s.empty()) {
			while (cptr) {
				s.push(cptr);
				cptr = cptr->left;
			}
			cptr = s.top();
			s.pop();
			if (cptr->val > m) m = cptr->val;
			else return false;
			cptr = cptr->right;
		}
		
		return true;
    }
};
