// Run Time 16ms

class Solution {
public:
    string convert(string s, int numRows) {
		string ans("");
		int j, jj, sLen = s.size(), oddStep, evenStep;

		if (numRows == 1) {return s;}
		for (int i = 1; i <= numRows; ++i) {
			evenStep = numRows*2 - 2;
			j = i;
			if ((i == 1) || (i == numRows)) {
				while (j <= sLen) {
					ans += s[j-1];
					j += evenStep;
				}
			}
			else {
				oddStep = (numRows - i)*2;
				while (j <= sLen) {
					ans += s[j-1];
					jj = j + oddStep;
					if (jj > sLen) {break;}
					ans += s[jj-1];
					j += evenStep;
				}
			}
		}

		return ans;
    }
}; 
