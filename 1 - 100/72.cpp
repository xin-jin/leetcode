// Run time 13ms

class Solution {
public:
    int minDistance(string word1, string word2) {
		vector<vector<int>> dis(word1.size(), vector<int>(word2.size(), -1));
		return calDis(word1, word2, dis, word1.size() - 1, word2.size() - 1);
    }

private:
	int calDis(string& word1, string& word2, vector<vector<int>>& dis, int end1, int end2) {
		if (end1 == -1 || end2 == -1) return max(end1+1, end2+1);
		if (dis[end1][end2] != -1) return dis[end1][end2];
		int ret;
		if (word1[end1] == word2[end2]) {
			ret = calDis(word1, word2, dis, end1-1, end2-1);
		}
		else {
			ret = min(min(calDis(word1, word2, dis, end1-1, end2-1),
						  calDis(word1, word2, dis, end1, end2-1)),
					  calDis(word1, word2, dis, end1-1, end2)) + 1;
		}
		dis[end1][end2] = ret;
		return ret;
	}
};
