// Run Time 12ms
// Generalization to Permutation II is probably inefficient

class Solution {
public:
    vector<vector<int>> permute(vector<int>& nums) {
		vector<vector<int>> ans;

		permute(ans, nums, 0);

		return ans;
    }

private:
	void permute(vector<vector<int>>& ans, vector<int>& nums, int start) {
		if (start == nums.size()-1) ans.push_back(nums);
		else {
			for (auto i = start; i != nums.size(); ++i) {
				int tmp = nums[i];
				swap(nums[start], nums[i]);
				permute(ans, nums, start+1);
				swap(nums[start], nums[i]);
			}
		}
	}
};
