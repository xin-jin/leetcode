// Run Time 4ms

class Solution {
public:
    string countAndSay(int n) {
		string say = "1",  nextSay;
		
		for (int i = 1; i < n; ++i) {
			int j = 0;
			nextSay = "";
			while (say[j]) {
				int k = j;
				while (say[j] == say[k]) ++k;
				nextSay += to_string(k-j);
				nextSay.push_back(say[j]);
				j = k;
			}
			say = nextSay;
		}

		return say;
    }
};
