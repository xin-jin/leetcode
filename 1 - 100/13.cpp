// This is a slow implementation. A more elaborate solution could be more efficient.
// Run Time 80ms

#include <map>

class Solution {
public:
    int romanToInt(string s) {
		auto first = s.begin(), last = s.end();
		int cnt, ans(0);
		map<char, int> chmap{{'M', 1000}, {'D', 500}, {'C', 100}, {'L', 50},
							 {'X', 10}, {'V', 5}, {'I', 1}};
		map<string, int> sub_chmap{{"CM", 200}, {"CD", 200}, {"XC", 20}, {"XL", 20},
					  {"IX", 2}, {"IV", 2}};

		for (const auto &ch:chmap) {
			cnt = count(first, last, ch.first);
			ans += cnt*(ch.second);
		}

		for (const auto &ch:sub_chmap) {
			if (s.find(ch.first) != string::npos) {ans -= ch.second;}
		}

		return ans;
    }
};
