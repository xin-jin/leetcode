// Run Time 8ms

class Solution {
public:
    int trap(vector<int>& height) {
		stack<int> leftmax, rightmax;
		int ans = 0;

		if (!height.size()) return 0;
		leftmax.push(0);
		for (int i = 1; i != height.size(); ++i)
			if (height[i] > height[leftmax.top()]) leftmax.push(i);
		for (int i = height.size()-1; i != leftmax.top(); --i)
			if (rightmax.empty() || height[i] > height[rightmax.top()]) rightmax.push(i);
		int j = leftmax.top();
		while (!rightmax.empty()) {
			ans += (rightmax.top()-j-1)*height[rightmax.top()]
				- accumulate(height.begin()+j+1, height.begin()+rightmax.top(), 0);
			j = rightmax.top();
			rightmax.pop();
		}
		j = leftmax.top();
		leftmax.pop();
		while (!leftmax.empty()) {
			ans += (j-leftmax.top()-1)*height[leftmax.top()]
				- accumulate(height.begin()+leftmax.top()+1, height.begin()+j, 0);
			j = leftmax.top();
			leftmax.pop();
		}		

		return ans;
    }
};
