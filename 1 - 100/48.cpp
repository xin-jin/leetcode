// Run time 4ms
// There is also a very clever method that reverses then transposes

class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
		if (!matrix.size()) return;
		int n = matrix.size() - 1;

		for (int i = 0; i <= n/2; ++i)
			for (int j = 0; j < (n+1)/2; ++j) {
				swap(matrix[i][j], matrix[j][n-i]);
				swap(matrix[i][j], matrix[n-i][n-j]);
				swap(matrix[i][j], matrix[n-j][i]);
			}		
    }
};
