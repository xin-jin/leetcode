// Run Time 16ms
// Works for Permutation II as well

class Solution {
public:
    vector<vector<int>> permute(vector<int>& nums) {
		vector<vector<int>> ans;
		vector<int> acc {};

		sort(nums.begin(), nums.end());
		permute(ans, nums, acc);

		return ans;
    }

private:
	void permute(vector<vector<int>>& ans, vector<int>& unused, vector<int>& acc) {
		if (unused.size() == 1) {
			acc.push_back(unused[0]);
			ans.push_back(acc);
			acc.erase(acc.end()-1);
		}
		else {
			for (auto i = unused.begin(); i != unused.end(); ) {
				int tmp = *i;
				acc.push_back(tmp);
				unused.erase(i);
				permute(ans, unused, acc);
				unused.insert(i, tmp);
				acc.erase(acc.end()-1);
				while (i != unused.end() && *i == tmp) ++i;
			}
		}
	}
};
