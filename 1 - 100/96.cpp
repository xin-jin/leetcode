// Run Time 0ms

class Solution {
public:
    int numTrees(int n) {
		if (n == 1) return 1;
        vector<int> ans {1, 1};

		for (int i = 1; i < n; ++i) {
			int tmp = 0;
			for (int j = 0; j < (i+1)/2; ++j) tmp += ans[j] * ans[i-j];
			tmp *= 2;
			if (!(i & 1)) tmp += ans[i/2]*ans[i/2];
			ans.push_back(tmp);
		}

		return ans[n];
    }
};
