// Run Time 6ms

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        unordered_map<int, int> diff;
        for (int i = 0; i != nums.size(); ++i) {
            auto rnt = diff.find(nums[i]);
            if (rnt != diff.end())
                return {i, rnt->second};
            else
                diff[target - nums[i]] = i;
        }
        return {};
    }
};
