// Run time 0ms

class Solution {
public:
    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        vector<vector<int>> ans;
        vector<int> acc;
        genSet(nums, ans, 0, acc);
        return ans;
    }
    
private:
    void genSet(vector<int>& nums, vector<vector<int>>& ans, int s, vector<int>& acc) {
        if (s >= nums.size()) {
            ans.push_back(acc);
            return;
        }
        int next_pos = s + 1;
        while (next_pos < nums.size() && nums[next_pos] == nums[s])
            ++next_pos;
        genSet(nums, ans, next_pos, acc);
        for (int i = s; i != next_pos; ++i) {
            acc.push_back(nums[i]);
            genSet(nums, ans, next_pos, acc);
        }
        acc.erase(acc.end()-(next_pos-s), acc.end());
    }
};
