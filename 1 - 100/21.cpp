// Run Time 12ms

class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
		auto *head = l1;

		if (!l1) return l2;
		if (!l2) return l1;
		if (l1->val > l2->val) {
			head = l2;
			l2 = l2->next;
			head->next = l1;
		}

		auto *prev = head;
		l1 = head->next;
		while (l2) {
			if (!l1) { prev->next = l2; break; }
			if (l1->val > l2->val) {
				prev->next = l2;
				l2 = l2->next;
				prev->next->next = l1;
				prev = prev->next;
			}
			else { prev = l1; l1 = l1->next;}
		}		

		return head;
    }
}; 
