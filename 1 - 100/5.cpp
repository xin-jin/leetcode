// Run time 6ms

class Solution {
public:
    string longestPalindrome(string s) {
		if (s.size() == 0) return "";

		int s_size = s.size(), ans_size = 1, left, right;
		string ans(1, s[0]);

		for (int i = 0; i < s_size;) {
			left = right = i;
			while (right+1 < s_size && s[right+1] == s[right]) ++right;
			i = right + 1;
			while (left-1 >= 0 && right+1 < s_size && s[left-1] == s[right+1]) { --left; ++right; }
			if (right+1-left > ans_size) {
				ans_size = right+1-left;
				ans = s.substr(left, ans_size);
			}
		}

		return ans;
    }
};
