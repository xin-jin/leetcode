// Run Time 4ms

class Solution {
public:
    string addBinary(string a, string b) {
		string ans;
		int carry = 0, tmp;

		if (a.size() < b.size()) swap(a, b);
		int aLast = a.size()-1, bLast = b.size()-1;
		for (int i = 0; i <= aLast; ++i) {
			if (i <= bLast) tmp = a[aLast-i] + b[bLast-i] - 2*'0' + carry;
			else tmp = a[aLast-i] - '0' + carry;
			carry = tmp / 2;
			ans.push_back((tmp & 1) + '0');
		}
		if (carry) ans.push_back('1');
		reverse(ans.begin(), ans.end());

		return ans;
    }
};
