// Run time 6ms

class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {
		vector<vector<int>> ans;
		vector<int> acc;
		
		genSubsets(ans, nums, acc, 0);

		return ans;
    }

private:
	void genSubsets(vector<vector<int>>& ans, vector<int>& nums, vector<int>& acc, int start) {
		if (start >= nums.size()) {
		    ans.push_back(acc);
		    return;
		}
		
		genSubsets(ans, nums, acc, start+1);
		acc.push_back(nums[start]);
		genSubsets(ans, nums, acc, start+1);
		acc.pop_back();
	}
};
