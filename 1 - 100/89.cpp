// Run Time 4ms

class Solution {
public:
    vector<int> grayCode(int n) {
		vector<int> ans {0, 1};

		if (!n) return vector<int>({0});
		for (int i = 2, bits = 2; i <= n; ++i, bits = bits << 1) {
			for (int j = ans.size()-1; j >= 0; --j)
				ans.push_back(ans[j] | bits);
		}
		
		return ans;
    }
};
