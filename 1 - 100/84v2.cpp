// Run Time 24ms
// Stack implementation

class Solution {
public:
    int largestRectangleArea(vector<int>& height) {
		int n = height.size(), ans = 0;
		stack<int> hStack;

		height.insert(height.begin(), -1);
		hStack.push(0);
		height.push_back(-1);
		for (int i = 1; i != n+2; ++i) {
			while (height[i] < height[hStack.top()]) {
				int h = height[hStack.top()];
				hStack.pop();
				int area = h * (i-hStack.top()-1);
				if (area > ans) ans = area;
			}
			hStack.push(i);
		}
		
		return ans;
    }
};
