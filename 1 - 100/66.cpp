// Run Time 4ms

class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
		auto i = digits.end() - 1;

		*i += 1;
		while (i != digits.begin() && *i >= 10) {
			*i -= 10;
			i -= 1;
			*i += 1;
		}
		if (*i >= 10) {
			*i -= 10;
			digits.insert(i, 1);
		}

		return digits;
    }
};
