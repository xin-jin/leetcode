// Run Time 12ms

class Solution {
public:
    int maxSubArray(vector<int>& nums) {
		int maxContSum = INT_MIN, ans = INT_MIN;
		
        for (int i = 0; i != nums.size(); ++i) {
			if (maxContSum > 0) maxContSum += nums[i];
			else maxContSum = nums[i];
			if (maxContSum > ans) ans = maxContSum;
		}

		return ans;
    }
};
