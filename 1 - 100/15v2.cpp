// Run Time 52ms

class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
		vector<vector<int>> ans;
		int tmp, a, b, c;

		if (nums.size() < 3) {return ans;}
		sort(nums.begin(), nums.end());
		auto i = nums.cbegin(), last = nums.cend(), j = i, k = i;
		for (i = nums.cbegin(); i < last-2;) {
			j = i+1; k = last - 1;
			a = *i;
			do {
				b = *j; c = *k; tmp = a+b+c;			   
				if (tmp == 0) {ans.push_back(vector<int>{a, b, c});}
				if (tmp >= 0) {while (j < k && *k == c) {--k;};}					
				if (tmp <= 0) {while (j < k && *j == b) {++j;};}
			} while (j < k);
			while (i < last-2 && *i == a) {++i;}
		}

		return ans;
    }
};
