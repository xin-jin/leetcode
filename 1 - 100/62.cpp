// Run Time 0ms

class Solution {
public:
    int uniquePaths(int m, int n) {
		vector<vector<int>> mp(m, vector<int>(n, 0));

		mp[0][0] = 1;		

		return countPaths(m-1, n-1, mp);
    }
private:
	int countPaths(int ci, int cj, vector<vector<int>>& mp) {
		if (mp[ci][cj] > 0) return mp[ci][cj];

		int left(0), up(0);
		if (ci>0) left = countPaths(ci-1, cj, mp);
		if (cj>0) up = countPaths(ci, cj-1, mp);
		mp[ci][cj] = left+up;
		return mp[ci][cj];
	}
}; 
