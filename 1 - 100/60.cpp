// Run Time 0ms

class Solution {
public:
    string getPermutation(int n, int k) {
		vector<int> unused(n);
		int fac = 1;
		string ans;
		
		iota(unused.begin(), unused.end(), 1);
		for (int i = 2; i < n; ++i) fac *= i;
		getPermutation(ans, unused, n, k, n-1, fac);

		return ans;
    }

private:
	void getPermutation(string& ans, vector<int>& unused, const int& n, int k, int i, int fac) {
		if (!i) { ans.push_back(unused[0]+'0'); return; }
		int div = (k-1) / fac;
		ans.push_back(unused[div]+'0');
		unused.erase(unused.begin()+div);
		getPermutation(ans, unused, n, (k-1) % fac + 1, i-1, fac/i);
	}
};
