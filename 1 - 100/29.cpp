// Run Time 8ms

class Solution {
public:
    int divide(int dividend, int divisor) {
		bool isPos = false;

		if (divisor == 0) {return INT_MAX;}
		if (dividend == INT_MIN) {
			if (divisor == 1) {return INT_MIN;}
			if (divisor == -1) {return INT_MAX;}
		}
		if (dividend == 0) {return 0;}
		if ((dividend>0 && divisor>0) ||
			(dividend<0 && divisor<0)) {isPos = true;}
		if (divisor == INT_MIN) {
			if (dividend == INT_MIN) {return 1;}
			else {return 0;}
		}
		unsigned int c = dividend>0 ? dividend : -dividend, d = abs(divisor);

		return isPos ? rawDivide(c, d) : -rawDivide(c, d);
    }
private:
	unsigned int rawDivide(unsigned int c, unsigned int d) {
		if (c < d) {return 0;}
		if (d == 1) {return c;}
		unsigned int cdiv2 = (c >> 1) + 1, dd = d, power = 0;

		while (dd < cdiv2) {dd = dd << 1; ++power;}

		return (1 << power) + rawDivide(c - dd, d);
	}
}; 
