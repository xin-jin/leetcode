// Run Time 36ms

class Solution {
public:
    bool exist(vector<vector<char>>& board, string word) {
		if (!word.size()) return true;
		if (!board.size()) return false;
		if (word.size() > board.size() * board[0].size()) return false;
        vector<vector<bool>> isVisited(board.size(), vector<bool>(board[0].size(), false));

		for (int i = 0; i != board.size(); ++i)
			for (int j = 0; j != board[0].size(); ++j)
				if (board[i][j] == word[0] && dfs(isVisited, board, word, i, j, 1)) return true;
		
		return false;
    }
	
private:
	bool dfs(vector<vector<bool>>& isVisited, const vector<vector<char>>& board, const string& word, 
			 int i, int j, int k) {
		if (k == word.size()) return true;
		isVisited[i][j] = true;
		if (i > 0 && !isVisited[i-1][j] && board[i-1][j] == word[k] && dfs(isVisited, board, word, i-1, j, k+1)) return true;
		if (j > 0 && !isVisited[i][j-1] && board[i][j-1] == word[k] && dfs(isVisited, board, word, i, j-1, k+1)) return true;
		if (i < board.size()-1 && !isVisited[i+1][j] && board[i+1][j] == word[k] && dfs(isVisited, board, word, i+1, j, k+1)) return true;
		if (j < board[0].size()-1 && !isVisited[i][j+1] && board[i][j+1] == word[k] && dfs(isVisited, board, word, i, j+1, k+1)) return true;
		isVisited[i][j] = false;

		return false;
	}
};
