// Run Time 16ms

class Solution {
public:
    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
		vector<vector<int>> ans;
		vector<int> acc;

		sort(candidates.begin(), candidates.end());
		bfs(ans, candidates, target, 0, acc);

		return ans;
    }

private:
	void bfs(vector<vector<int>>& ans, vector<int>& can, int diff, int i, vector<int>& acc) {
		if (i == can.size() || diff < can[i]) return;
		acc.push_back(can[i]);
		if (diff == can[i]) { ans.push_back(acc); acc.pop_back(); }
		else {
			bfs(ans, can, diff-can[i], i, acc);
			acc.pop_back();
			bfs(ans, can, diff, i+1, acc);
		}
	}
};
