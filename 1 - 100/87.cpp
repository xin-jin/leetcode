// Run Time 4ms

class Solution {
public:
    bool isScramble(string s1, string s2) {
		int l = s1.size();				
		if (l != s2.size()) return false;
		if (s1 == s2) return true;
		if (!isRearrange(s1, s2)) return false;
		if (l <= 3) return true;

		for (int j = 1; j < l; ++j) {
			string s11 = s1.substr(0, j), s12 = s1.substr(j, l-j);
			if (isScramble(s11, s2.substr(0, j)) && isScramble(s12, s2.substr(j, l-j))) return true;
			if (isScramble(s11, s2.substr(l-j, j)) && isScramble(s12, s2.substr(0, l-j))) return true;
		}

		return false;
    }

private:
	bool isRearrange(const string& s1, const string& s2) {
		array<int, 26> cnt {};
		
		for (int i = 0; i < s1.size(); ++i) ++cnt[s1[i]-97];
		for (int i = 0; i < s2.size(); ++i)
			if (cnt[s2[i]-97] <= 0) return false;
			else --cnt[s2[i]-97];
		return true;
	}
};
