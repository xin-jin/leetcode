// Run Time 8ms

class Solution {
public:
    bool search(vector<int>& nums, int target) {
		return search(nums, target, 0, nums.size()-1, false);
    }

private:
	bool search(vector<int>& nums, int target, int i, int j, bool sorted) {
		const int mid = (i+j)/2;

		if (j-i <= 1) {
			if (nums[i] == target || nums[j] == target) return true;
			else return false;
		}

		if (target == nums[mid]) return true;		
		if (sorted) {
			if (target > nums[j] || target < nums[i]) return false;
			if (target < nums[mid]) return search(nums, target, i, mid-1, true);
			else return search(nums, target, mid+1, j, true);
		}
		else {
			bool lreturn;
			if (nums[i] < nums[mid-1]) lreturn = search(nums, target, i, mid-1, true);
			else lreturn = search(nums, target, i, mid-1, false);
			if (lreturn) return true;
			if (nums[mid+1] < nums[j]) return search(nums, target, mid+1, j, true);
			else return search(nums, target, mid+1, j, false);			
		}
	}
};
