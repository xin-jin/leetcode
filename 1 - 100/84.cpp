// Run Time 16ms

class Solution {
public:
    int largestRectangleArea(vector<int>& height) {
		int n = height.size(), ans = 0;
		vector<int> bLeft(n), bRight(n);

		for (int i = 0; i != n; ++i) {
			int j = i - 1;
			while (j >= 0 && height[j] >= height[i]) j = bLeft[j];
			bLeft[i] = j;
		}
		for (int i = n-1; i >= 0; --i) {
			int j = i + 1;
			while (j < n && height[j] >= height[i]) j = bRight[j];
			bRight[i] = j;
		}
		for (int i = 0; i != n; ++i) ans = max(ans, height[i]*(bRight[i]-bLeft[i]-1));
		
		return ans;
    }
};
