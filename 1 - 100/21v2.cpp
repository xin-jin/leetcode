// Run Time 8ms

class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
		ListNode head(0), *cptr = &head;

		while (l1 && l2) {
			if (l1->val > l2->val) { cptr->next = l2; l2 = l2->next; }
			else { cptr->next = l1; l1 = l1->next; }
			cptr = cptr->next;
		}
		cptr->next = (l1 ? l1 : l2);

		return head.next;
    }
};
