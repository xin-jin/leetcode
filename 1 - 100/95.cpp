// Run Time 24ms

class Solution {
public:
    vector<TreeNode*> generateTrees(int n) {
		if (!n) return vector<TreeNode*>({NULL});
		return generateTrees(1, n);
    }

private:
	vector<TreeNode*> generateTrees(int s, int t) {
		if (s == t) {
			TreeNode *newNode = new TreeNode(s);
			return vector<TreeNode*>({newNode});
		}
		vector<TreeNode*> ans;

		auto rightTree = generateTrees(s+1, t);
		for (auto i = rightTree.begin(); i != rightTree.end(); ++i) {
			TreeNode *newNode = new TreeNode(s);
			newNode->right = *i;
			ans.push_back(newNode);
		}
		
		auto leftTree = generateTrees(s, t-1);
		for (auto i = leftTree.begin(); i != leftTree.end(); ++i) {
			TreeNode *newNode = new TreeNode(t);
			newNode->left = *i;
			ans.push_back(newNode);
		}
		
		for (int i = s+1; i <= t-1; ++i) {
			auto leftTree = generateTrees(s, i-1),
				rightTree = generateTrees(i+1, t);
			for (auto j = leftTree.begin(); j != leftTree.end(); ++j)
				for (auto k = rightTree.begin(); k != rightTree.end(); ++k) {
					TreeNode *newNode = new TreeNode(i);
					newNode->left = *j;
					newNode->right = *k;
					ans.push_back(newNode);
				}
		}
		
		return ans;
	}
};
