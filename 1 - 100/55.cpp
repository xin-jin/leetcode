// Run Time 16ms

class Solution {
public:
    bool canJump(vector<int>& nums) {
		int maxPos = 0;

		for (int i = 0; ; ++i) {
			if (i > maxPos) return false;
			if (i == nums.size()-1) break;
			if (i + nums[i] > maxPos) maxPos = i + nums[i];
		}

		return true;
    }
};
