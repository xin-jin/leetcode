// Run Time 16ms

class Solution {
public:
    int longestValidParentheses(string s) {
		stack<int> ss, prevMatch;
		int ans = 0;

		for (int i = 0; i != s.size(); ++i) {
			if (s[i] == '(') ss.push(i);
			else if (ss.empty()) prevMatch = stack<int>();
			else {
				int tmp = ss.top();
				ss.pop();
				while (!prevMatch.empty() && prevMatch.top() > tmp) prevMatch.pop();
				while (!prevMatch.empty() && (ss.empty() || prevMatch.top() > ss.top())) {
					tmp = prevMatch.top();
					prevMatch.pop();
				}
				ans = max(ans, i + 1 - tmp);
				prevMatch.push(tmp);
			}
		}

		return ans;
    }
};
