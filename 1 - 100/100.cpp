// Run Time 0ms

class Solution {
public:
    bool isSameTree(TreeNode* p, TreeNode* q) {
		if (p && q && p->val == q->val) return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
		else if (!p && !q) return true;
		else return false;
    }
};
