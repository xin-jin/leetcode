// Run Time 12ms

class Solution {
public:
    int longestValidParentheses(string s) {
        stack<int> ss;
		int j = s.size(), ans = 0;

		for (int i = 0; i != s.size(); ++i)
			if (s[i] == '(') ss.push(i);
			else if (ss.empty() || s[ss.top()] == ')') ss.push(i);
			else ss.pop();
		while (!ss.empty()) {
			ans = max(ans, j - ss.top() - 1);
			j = ss.top();
			ss.pop();
		}
		ans = max(ans, j);

		return ans;
    }
};
