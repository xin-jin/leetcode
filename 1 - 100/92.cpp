// Run Time 4ms

class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int m, int n) {
		ListNode dummy(0), *s = &dummy;
		dummy.next = head;
		if (m == n) return head;
		
		for (int i = 1; i < m ; ++i) s = s->next;
		ListNode *prev = s->next, *cptr = prev->next;
		for (int i = m+1; i <= n; ++i) {
			ListNode *tmp = cptr->next;
			cptr->next = prev;
			prev = cptr;
			cptr = tmp;
		}
		s->next->next = cptr;
		s->next = prev;

		return dummy.next;
    }
};
