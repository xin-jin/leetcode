// Run Time 8ms

class Solution {
public:
    int myAtoi(string str) {
		int i = 0, chval;
		long long ans = 0;
		bool isNeg = false;

		if (str == "") {return 0;}
		while (str[i] == ' ') {++i;} // remove the heading white spaces

		if (str[i] == '-') {
			isNeg = true;
			i++;
		}
		else if (str[i] == '+') {++i;}

		chval = int(str[i]);
		if ((chval >= 48) && (chval <= 57)) {
			ans = chval - 48;
			i++;
		}
		else {return 0;}
		for (; i < str.length(); ++i) {
			chval = int(str[i]);
			if ((chval < 48) || (chval > 57)) {return (isNeg) ? (-ans) : (ans);}
			ans = ans*10 + chval - 48;
			if (!isNeg && (ans > INT_MAX)) {return INT_MAX;}
			else if (isNeg && (-ans < INT_MIN)) {return INT_MIN;}
		}

		return (isNeg) ? (-ans) : (ans);
    }
};
