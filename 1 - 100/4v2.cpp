//  Run time 29ms

class Solution {
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        int total = nums1.size() + nums2.size();
        if (total & 1)
            return findKth(nums1, nums2, 0, 0, total / 2);
        else
            return (findKth(nums1, nums2, 0, 0, total / 2) + findKth(nums1, nums2, 0, 0, total / 2 - 1)) / 2;
    }
    
private:
    double findKth(const vector<int>& nums1, const vector<int>& nums2, int s1, int s2, int k) {
        if (s1 >= nums1.size()) return nums2[s2+k];
        if (s2 >= nums2.size()) return nums1[s1+k];
        if (k == 0) return min(nums1[s1], nums2[s2]);
        int pos1 = min(int(nums1.size()-1), s1 + (k-1)/2);
        int pos2 = min(int(nums2.size()-1), s2 + (k-1)/2);
        if (nums1[pos1] < nums2[pos2])
            return findKth(nums1, nums2, pos1+1, s2, k - (pos1+1-s1));
        else
            return findKth(nums1, nums2, s1, pos2+1, k - (pos2+1-s2));
    }
};  
