// Run Time 4ms

class Solution {
public:
    int numDecodings(string s) {
		int current = 0, prev = 1;

		if (!s.size()) return 0;
		if (s.back() == '0') current = 0;
		else current = 1;
		for (int i = s.size()-2; i >= 0; --i) {
			if (s[i] == '0') {
				prev = current;
				current = 0;
			}
			else if (s[i] < '2' || (s[i] == '2' && s[i+1] <= '6')) {
				current = current + prev;
				prev = current - prev;
			}
			else {
				prev = current;
				current = current;
			}
		}

		return current;
	}
};
