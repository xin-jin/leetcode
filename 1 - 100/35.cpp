// Run Time 6ms

class Solution {
public:
    int searchInsert(vector<int>& nums, int target) {
        int s = 0, t = nums.size() - 1;
        while (s <= t) {
            int mid = s+ (t-s)/2;
            if (target == nums[mid]) return mid;
            if (target > nums[mid]) s = mid+1;
            else t = mid-1;
        }
        
        return s;
    }
};
