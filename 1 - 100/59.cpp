// Run Time 4ms

class Solution {
public:
    vector<vector<int>> generateMatrix(int n) {
		vector<vector<int>> ans(n, vector<int>(n, 0));
		vector<int> bi {0, n-1}, bj {0, n-1};
		int current = 1;

		outputSpiral(ans, bi, bj, current);

		return ans;
    }

private:
	void outputSpiral(vector<vector<int>>& ans, vector<int>& bi, vector<int>& bj, int& current) {
		int i = bi[0], j = bj[0];

		if (bi[0] == bi[1]) { ans[i][j] = current; return; }
		if (bi[0] > bi[1]) return;
		
		for ( ; j < bj[1]; ++j, ++current) ans[i][j] = current;
		for (i = bi[0], j = bj[1]; i < bi[1]; ++i, ++current) ans[i][j] = current;
		for (i = bi[1], j = bj[1]; j > bi[0]; --j, ++current) ans[i][j] = current;
		for (i = bi[1], j = bj[0]; i > bi[0]; --i, ++current) ans[i][j] = current;

		bi = {bi[0]+1, bi[1]-1}; bj = {bj[0]+1, bj[1]-1};

		outputSpiral(ans, bi, bj, current);
	}
};
