// For the purpose of this problem, brute force is sufficient. A much faster solution would be the KMP algorithm.
// Run Time 4ms

class Solution {
public:
    int strStr(string haystack, string needle) {
		int h_size = haystack.size(), n_size = needle.size();

		for (int i = 0; i <= h_size - n_size; ++i) {
			bool isMatch = true;
			for (int j = 0; j < n_size; ++j) {
				if (haystack[i+j] != needle[j]) { isMatch = false; break; }
			}
			if (isMatch) return i;
		}

		return -1;
    }
};
