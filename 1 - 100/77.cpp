// Run Time 8ms

class Solution {
public:
    vector<vector<int>> combine(int n, int k) {
		vector<vector<int>> ans;
		vector<int> acc;
		genCombine(ans, acc, 1, 1, k, n);

		return ans;
    }

private:
	void genCombine(vector<vector<int>>& ans, vector<int>& acc, int i, int start, const int& k, const int& n) {
		if (i > k) ans.push_back(acc);
		else for (int j = start; k-i <= n-j; ++j) {
			acc.push_back(j);
			genCombine(ans, acc, i+1, j+1, k, n);
			acc.erase(acc.end()-1);
		}
	}
};
