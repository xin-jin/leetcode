// Run time 9ms

class Solution {
public:
    string minWindow(string s, string t) {
        vector<int> cnt(128, 0);
        for (char c : t) --cnt[c];
        int min_len = INT_MAX, head = 0, min_head, total_cnt = t.size();
        for (int tail = 0; tail != s.size(); ++tail) {
            if (cnt[s[tail]] < 0) --total_cnt;
            ++cnt[s[tail]];
            if (total_cnt == 0) {
                while (true) {
                    if (cnt[s[head]] == 0) break;
                    --cnt[s[head]];
                    ++head;
                }
                if (tail + 1 - head < min_len) {
                    min_head = head;
                    min_len = tail + 1 - head;
                }
                --cnt[s[head]];
                ++head;
                ++total_cnt;
            }
        }
        
        return min_len < INT_MAX ? s.substr(min_head, min_len) : "";
    }
};
