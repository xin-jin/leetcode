class Solution(object):
    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        if s == "": return 0
        cur, prev = 1, 0
        n = len(s)
        for i in range(n):
            if s[i] == '0':
                if i == 0 or (i > 0 and s[i-1] > '2'): return 0
                cur, prev = prev, 0
            elif i > 0 and s[i-1:i+1] <= "26":
                cur, prev = prev + cur, cur
            else:
                prev = cur
                
        return cur
