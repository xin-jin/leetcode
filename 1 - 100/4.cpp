// Run Time 44ms

class Solution {
	using iter_type = vector<int>::iterator;
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
		const int ts = nums1.size() + nums2.size();

		if (ts & 1) return kthNum(nums1.begin(), nums1.end(), nums2.begin(), nums2.end(), (ts+1)/2);
		else return (kthNum(nums1.begin(), nums1.end(), nums2.begin(), nums2.end(), ts/2) +
				kthNum(nums1.begin(), nums1.end(), nums2.begin(), nums2.end(), ts/2+1))/2;
    }
	
private:
	double kthNum(const iter_type& l1, const iter_type& r1, const iter_type& l2, const iter_type& r2, const int k) {
		const int s1 = r1 - l1, s2 = r2 - l2;

		if (s1 == 0) return *(l2+k-1);
		if (s2 == 0) return *(l1+k-1);
		if (k == 1) return min(*l1, *l2);
		
		auto m1 = l1 + (min(s1, k/2) - 1);
		auto m2 = l2 + (min(s2, k/2) - 1);
		if (*m1 <= *m2) return kthNum(m1+1, r1, l2, r2, k-(m1-l1+1));
		if (*m1 > *m2) return kthNum(l1, r1, m2+1, r2, k-(m2-l2+1));
	}
};
