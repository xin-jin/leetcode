// Run Time 16ms

class Solution {
public:
    int maximalRectangle(vector<vector<char>>& matrix) {
		if (!matrix.size()) return 0;
        int ans = 0;		
		
		vector<int> h(matrix[0].size(), 0);		
		for (int i = 0; i != matrix.size(); ++i) {
			for (int j = 0; j != matrix[0].size(); ++j)
				if (matrix[i][j] == '1') ++h[j];
				else h[j] = 0;
			int largestArea = largestRectangleArea(h);
			if (largestArea > ans) ans = largestArea;
		}

		return ans;
    }

private:	
    int largestRectangleArea(const vector<int>& height) {
		int n = height.size(), ans = 0;
		vector<int> bLeft(n), bRight(n);

		for (int i = 0; i != n; ++i) {
			int j = i - 1;
			while (j >= 0 && height[j] >= height[i]) j = bLeft[j];
			bLeft[i] = j;
		}
		for (int i = n-1; i >= 0; --i) {
			int j = i + 1;
			while (j < n && height[j] >= height[i]) j = bRight[j];
			bRight[i] = j;
		}
		for (int i = 0; i != n; ++i) ans = max(ans, height[i]*(bRight[i]-bLeft[i]-1));
		
		return ans;
    }
};
