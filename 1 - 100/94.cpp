// Run Time 0ms

class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
		vector<int> ans;
		stack<TreeNode*> s;

		TreeNode *cptr = root;
		while (cptr || !s.empty()) {
			while (cptr) {
				s.push(cptr);
				cptr = cptr->left;
			}
			cptr = s.top();
			s.pop();
			ans.push_back(cptr->val);
			cptr = cptr->right;
		}

		return ans;
    }
};
