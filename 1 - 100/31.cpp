// Run Time 12ms

class Solution {
public:
    void nextPermutation(vector<int>& nums) {
        auto i = nums.end()-1;
        while (i > nums.begin() && *(i-1) >= *i) --i;
        if (i > nums.begin()) {
            auto pos = lower_bound(i, nums.end(), *(i-1), greater<int>()) - 1;
            iter_swap(pos, i-1);
        }
        reverse(i, nums.end());
    }
};
