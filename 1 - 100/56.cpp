// Run Time 580ms

class Solution {
public:
    vector<Interval> merge(vector<Interval>& intervals) {
		vector<Interval> ans;
		int last = intervals.size() - 1, i = 0;

		if (!intervals.size()) return intervals;
		sort(intervals.begin(), intervals.end(), [](Interval a, Interval b) { return a.start < b.start; });		
		for (int j = 1; j <= last; ) {
			if (intervals[j].start <= intervals[i].end) {				
				intervals[i].end = max(intervals[i].end, intervals[j].end);
				++j;
			}
			else {
				ans.push_back(intervals[i]);
				i = j;
				++j;
			}
		}
		ans.push_back(intervals[i]);

		return ans;
    }
};
