// Run Time 24ms

class Solution {
public:
    int maxArea(vector<int>& height) {
		int i = 0, j = height.size() - 1, ans(0), vol, hi, hj;

		while (i < j) {
			hi = height[i];
			hj = height[j];
			vol = min(hi, hj)*(j-i);
			if (vol > ans) {ans = vol;}
			if (hi <= hj) {while (height[i] <= hi) {++i;}}
			if (hi >= hj) {while (height[j] <= hj) {--j;}}
		}

		return ans;
    }
};
