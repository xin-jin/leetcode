// Run Time 12ms

#include <algorithm>

class Solution {
public:
    int reverse(int x) {
		string s = to_string(x);
		if (x >= 0) {std::reverse(s.begin(), s.end());}
		else {std::reverse(s.begin()+1, s.end());}
		long long xl = std::stoll(s);

		if ((xl <= INT_MAX) && (xl >= INT_MIN))
			{return static_cast<int>(xl);}
		else {return 0;}
    }
}; 
