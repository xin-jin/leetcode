// Run Time 16ms

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
		std::vector<int> cpos(256, -1);
		int left_iter(0), right_iter(0), max_len(0), char_code(0);

		while (right_iter < s.length()) {
			char_code = static_cast<int>(s[right_iter]);
			if (cpos[char_code] >= left_iter) {
				if (right_iter - left_iter > max_len) max_len = right_iter - left_iter;
				left_iter = cpos[char_code] + 1;
				cpos[char_code] = right_iter;
			}
			cpos[char_code] = right_iter;
			right_iter++;
		}
		if (right_iter - left_iter > max_len) max_len = right_iter - left_iter;

		return max_len;
    }
};
