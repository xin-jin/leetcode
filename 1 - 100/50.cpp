// Run Time 4ms

class Solution {
public:
    double myPow(double x, int n) {
		double ans = 1, squared_part = x;
		bool sign = n > 0;
		unsigned absn = abs(n);
		
		if (!n) return 1;
		while (absn > 1) {
			if (absn & 1) { ans *= squared_part; absn -= 1; }
			else { squared_part *= squared_part; absn /= 2; }
		}

		if (sign) return ans*squared_part;
		else return 1/ans/squared_part;
    }
};
