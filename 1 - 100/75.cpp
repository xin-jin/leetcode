// Run Time 4ms

class Solution {
public:
    void sortColors(vector<int>& nums) {
		int l = 0, r = nums.size()-1;

		for (int i = 0; i <= r; ) {
			if (nums[i] == 1 || l > i) ++i;
			else if (nums[i] == 0) { swap(nums[i], nums[l]); ++l; }
			else if (nums[i] == 2) { swap(nums[i], nums[r]); --r; }
		}
    }
};
