class Solution(object):
    def firstMissingPositive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n = len(nums)
        for i in range(n):
            while (nums[i] != i+1):
                if nums[i] <= n and nums[i] > 0:
                    if nums[i] == nums[nums[i]-1]:
                        break
                    else:
                        nums[nums[i]-1], nums[i] = nums[i], nums[nums[i]-1]
                else:
                    break
        for i in range(n):
            if (nums[i] != i+1):
                return i+1
        return n+1
