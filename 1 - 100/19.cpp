// Run Time 4ms

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
		ListNode *prevOfNthFromCurrent = head, *cptr = head;

		for (auto i = 1; i <= n; ++i) {cptr = cptr->next;}
		if (cptr == NULL) {return head->next;}
		cptr = cptr->next;
		while (cptr != NULL) {
			prevOfNthFromCurrent = prevOfNthFromCurrent->next;
			cptr = cptr->next;
		}
		prevOfNthFromCurrent->next = (prevOfNthFromCurrent->next)->next;

		return head;
    }
}; 
