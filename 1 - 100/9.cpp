// Run Time 72ms

class Solution {
public:
    bool isPalindrome(int x) {
		if (x<0) {return false;}

		int xLen = floor(log(x)/log(10)) + 1, y = x,
			divFac = pow(10, xLen-1), xl, xr;

		for (int i = 1; i <= xLen/2; ++i) {
			xl = y / divFac;
			xr = y % 10;
			if (xl != xr) {return false;}
			y = y % divFac;
			y /= 10;
			divFac /= 100;
		}

		return true;
	}
};
