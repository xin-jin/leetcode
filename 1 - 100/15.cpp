// Run Time 92ms

#include <unordered_map>
#include <unordered_set>

class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
		unordered_multimap<int, array<int, 2>> sum2;
		unordered_set<int> sum1;
		vector<vector<int>> ans;
		int cnt;

		sort(nums.begin(), nums.end());
		auto i = nums.begin(), j = i;
		while (i != nums.end()) {
			for (j = i+1; (j != nums.end()) && (*j == *i); j++);
			cnt = j - i;
			auto range = sum2.equal_range(-(*i));
			for (auto k = range.first; k != range.second; k++)
				{ans.push_back(vector<int>{(k->second)[0], (k->second)[1], *i});}
			for (auto k = sum1.begin(); k != sum1.end(); k++)
				{sum2.insert({*k+*i, array<int, 2>{*k, *i}});}
			if (cnt >= 2) {
				if (sum1.find(-2*(*i)) != sum1.end())
					{ans.push_back(vector<int>{-2*(*i), *i, *i});}
				else
					{sum2.insert({*i+*i, array<int, 2>{*i, *i}});}
			}
			if ((cnt >= 3) && (*i == 0))
				{ans.push_back(vector<int>(3, 0));}
			sum1.insert(*i);
			i = j;
		}

		return ans;
	}
};
