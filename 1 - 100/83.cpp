// Run Time 16ms

class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
		for (auto cptr = head; cptr; cptr = cptr->next)
			while (cptr->next && cptr->next->val == cptr->val) cptr->next = cptr->next->next;

		return head;
    }
};
