// Run Time 0m

class Solution {
public:
    vector<string> restoreIpAddresses(string s) {
		vector<string> ans;
		genIp(s, 1, 0, ans, "");

		return ans;
    }

private:
	void genIp(const string& s, int i, int cptr, vector<string>& ans, string cs) {
		if (i == 4) {
			string ss = s.substr(cptr, s.size()-cptr);
			if (ss.size() >= 2 && ((ss[0] == '0') || stoi(ss) > 255)) return;
			ans.push_back(cs+ss);
		}
		else {
			for (int j = 1; j != 4; ++j) {
				string ss = s.substr(cptr, j);
				int remainSize = s.size() - (cptr+j);
				if (j == 3 && stoi(ss) > 255) return;
				if (remainSize < 4 - i) return;
				if (j >= 2 && ss[0] == '0') return;
				if (remainSize > 3*(4-i)) continue;
				genIp(s, i+1, cptr+j, ans, cs+ss+".");
			}
		}
	}
};
