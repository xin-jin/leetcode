// Run Time 16ms

class Solution {
public:
    bool isMatch(string s, string p) {
		s_size = s.size(); p_size = p.size();
		vector<vector<bool>> isMatchCache(s_size+1, vector<bool>(p_size+1)),
			isVisit(s_size+1,  vector<bool>(p_size+1, false));

		return isMatch(s, p, 0, 0, isMatchCache, isVisit);
	}

private:
	int s_size, p_size;

	bool isMatch(string&s, string&p, int i, int j, vector<vector<bool>>& isMatchCache, vector<vector<bool>>& isVisit) {
		if (isVisit[i][j]) return isMatchCache[i][j];

		bool q;
		if (i == s_size && j == p_size) q = true;
		else if (j == p_size) q = false;
		else if (j < p_size-1 && p[j+1] == '*') {
			q = isMatch(s, p, i, j+2, isMatchCache, isVisit);
			if (!q) {
				int ii = i;
				while (ii < s_size && (p[j] == s[ii] || p[j] == '.')) {
					++ii;
					if (isMatch(s, p, ii, j+2, isMatchCache, isVisit)) { q = true; break; }
				}
			}
		}
		else {
			if (i == s_size) q = false;
			else if (p[j] == s[i] || p[j] == '.') q = isMatch(s, p, i+1, j+1, isMatchCache, isVisit);
			else q = false;
		}
		isVisit[i][j] = true;
		isMatchCache[i][j] = q;
		return q;
	}
};
