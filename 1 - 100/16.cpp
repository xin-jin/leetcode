// Run Time 16ms

class Solution {
public:
    int threeSumClosest(vector<int>& nums, int target) {
		int tmp, a, b, c, min_dist = INT_MAX, ans;

		sort(nums.begin(), nums.end());
		auto i = nums.cbegin(), last = nums.cend(), j = i, k = i;
		for (i = nums.cbegin(); i < last-2;) {
			j = i+1; k = last - 1;
			a = *i;
			do {
				b = *j; c = *k; tmp = a+b+c - target;			   
				if (tmp == 0) {return target;}
				if (abs(tmp) < min_dist) {
					min_dist = abs(tmp);
					ans = a+b+c;
				}
				if (tmp > 0) {while (j < k && *k == c) {--k;};}
				if (tmp < 0) {while (j < k && *j == b) {++j;};}
			} while (j < k);
			while (i < last-2 && *i == a) {++i;}
		}

		return ans;
    }
}; 
