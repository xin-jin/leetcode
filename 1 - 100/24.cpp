// Run Time 4ms

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
		if (!head || !head->next) return head;
		ListNode *newhead = head->next;
		_swapParis(NULL, head, head->next);
		
		return newhead;
    }

private:
	void _swapParis(ListNode* prev, ListNode* former, ListNode* latter) {
		former->next = latter->next;
		latter->next = former;
		if (prev) prev->next = latter;
		if (former->next && former->next->next) _swapParis(former, former->next, former->next->next);
	}
};
