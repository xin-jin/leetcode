// Run Time 4ms

class Solution {
public:
    vector<string> generateParenthesis(int n) {
		vector<string> ans;

		if (n) _genParen(0, n, "", ans);
		
		return ans;
    }

private:
	void _genParen(int unmatched, int remain, string s, vector<string>& ans) {
		if (unmatched == 0 && remain == 0) { ans.push_back(s); return; }
		if (unmatched > 0) _genParen(unmatched-1, remain, s+")", ans);
		if (remain > 0) _genParen(unmatched+1, remain-1, s+"(", ans);
	}
};
