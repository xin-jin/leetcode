// In terms of time complexity, this is not the best implementation. But with the pruning, it is probably the fastest in practice.
// Run Time 24ms

class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
		vector<vector<int>> ans;
		int a, b, c, d, tmp;

		if (nums.size() < 4) {return ans;}
		sort(nums.begin(), nums.end());
		auto i = nums.cbegin(), last = nums.cend(), j = i, k = i, s = i;
		for (i = nums.cbegin(); i < last-3;) {
			if (*i + *(i+1) + *(i+2) + *(i+3) > target) {break;}
			if (*i + *(last-1) + *(last-2) + *(last-3) < target) {++i; continue;}
			a = *i;
			for (j = i+1; j < last-2;) {
				if (*i + *j + *(j+1) + *(j+2) > target) {break;}
				if (*i + *j + *(last-2) + *(last-1) < target) {++j; continue;}
				k = j + 1; s = last - 1; b = *j;
				do {
					c = *k; d = *s; tmp = a+b+c+d - target;
					if (tmp == 0) {ans.push_back(vector<int>{a, b, c, d});}
					if (tmp <= 0) {while (k < s && *k == c) {++k;};}
					if (tmp >= 0) {while (k < s && *s == d) {--s;};}					
				} while (k < s);
				while (j < last-2 && *j == b) {++j;}
			}
			while (i < last-3 && *i == a) {++i;}
		}

		return ans;
    }
}; 
