// Use divide and conquer
// Run Time 26ms

class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
		if (!lists.size()) return NULL;
		return mergeKLists(lists, 0, lists.size()-1);
    }

private:
	ListNode* mergeKLists(vector<ListNode*>& lists, int i, int j) {
		if (i == j) return lists[i];
		return mergeTwoLists(mergeKLists(lists, i, (i+j)/2), mergeKLists(lists, (i+j)/2+1, j));
	}
	
	ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
		ListNode head(0), *cptr = &head;

		while (l1 && l2) {
			if (l1->val > l2->val) { cptr->next = l2; l2 = l2->next; }
			else { cptr->next = l1; l1 = l1->next; }
			cptr = cptr->next;
		}
		cptr->next = (l1 ? l1 : l2);

		return head.next;
	}
};
