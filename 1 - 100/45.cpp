// Run Time 12ms

class Solution {
public:
    int jump(vector<int>& nums) {
        int cur_jump = 0, next_jump = 0, ans = 0;
        for (int i = 0; i < nums.size(); ++i) {
            if (i > cur_jump) {
                cur_jump = next_jump;
                ans += 1;
            }
            next_jump = max(next_jump, i + nums[i]);
        }
        return ans;
    }
};
