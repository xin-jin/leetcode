// Run Time 0ms

class Solution {
public:
    vector<string> letterCombinations(string digits) {
		if (digits == "") return vector<string>(0);
		
		vector<string> ans;
		genString(digits, 0, ans, "");

		return ans;
    }

private:
	unordered_map<char, string> keyMap{{'2', "abc"}, {'3', "def"}, {'4', "ghi"}, {'5', "jkl"}, {'6', "mno"},
		{'7', "pqrs"}, {'8', "tuv"}, {'9', "wxyz"}, {'0', " "}};

	void genString(string& s, int pos, vector<string>& ans, string current) {
		if (pos == s.size()) {
			ans.push_back(current);
			return;
		}
		for (char i:keyMap[s[pos]]) genString(s, pos+1, ans, current+string(1, i));
	}
};
