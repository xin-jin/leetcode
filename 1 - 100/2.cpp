// Run Time 36ms

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
		ListNode *newNode, *initNode = new ListNode(l1->val + l2->val), *cNode = initNode;
		int val1, val2;
		bool overTen = (initNode->val >= 10);

		l1 = l1->next;
		l2 = l2->next;
		while (l1 || l2) {
			val1 = (l1) ? (l1->val) : 0;
			val2 = (l2) ? (l2->val) : 0;
			newNode = new ListNode(val1 + val2);
			if (overTen) {
				newNode->val += 1;
				cNode->val -= 10;
			}
			overTen = (newNode->val >= 10);
			cNode->next = newNode;
			cNode = newNode;
			if (l1) {l1 = l1->next;}
			if (l2) {l2 = l2->next;}
		}
		if (overTen) {
			newNode = new ListNode(1);
			cNode->next = newNode;
			cNode->val -= 10;
		}

		return initNode;
    }
};
