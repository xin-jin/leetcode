// Run Time 22ms

class Solution {
public:
    string longestPalindrome(string s) {
        int max_len = 0;
        string::const_iterator best_left, best_right;
        for (auto i = s.cbegin(); i != s.cend(); ++i) {
            auto left = i, right = i+1;
            while (right != s.cend() && *(right-1) == *right) ++right;
            while (left > s.cbegin() && right < s.cend() && *(left-1) == *right) {
                --left;
                ++right;
            }
            if (right - left > max_len) {
                max_len = right - left;
                best_left = left;
                best_right = right;
            }
        }
        return string(best_left, best_right);
    }
}; 
