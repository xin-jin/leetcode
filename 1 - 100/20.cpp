// Run Time 0ms

class Solution {
public:
    bool isValid(string s) {
		vector<char> charList = {'(', '[', '{', ')', ']', '}'};
        stack<char> charStack;
		auto last = s.end();

		for (auto i = s.begin(); i != last; ++i) {
			bool found = false;
			for (auto j = 0; j <= 2; ++j)
				if (*i == charList[j]) {
					found = true;
					charStack.push(*i);
					break;
				}
			if (found) continue;
			for (auto j = 3; j <= 5; ++j) {
				if (*i == charList[j]) {
					if (!charStack.empty() && charStack.top() == charList[j-3]) charStack.pop();
					else return false;
				}
			}
		}

		if (charStack.empty()) return true;
		else return false;
    }
};
