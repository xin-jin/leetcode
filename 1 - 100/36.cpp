// Run Time 16ms

class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
		for (int i = 0; i <= 8; ++i)
			if (!isValidRow(board, i) || !isValidColumn(board, i)) return false;
		for (int i = 0; i <= 2; ++i)
			for (int j = 0; j <= 2; ++j)
				if (!isValidBlock(board, i, j)) return false;

		return true;
    }

private:
	bool isValidRow(vector<vector<char>>& board, int i) {
		vector<bool> visited(9, false);

		for (int j = 0; j <= 8; ++j)
			if (board[i][j] == '.') continue;
			else if (board[i][j] <= 48 || board[i][j] > 57) return false;
			else if (visited[board[i][j]-49]) return false;
			else visited[board[i][j]-49] = true;

		return true;
	}

	bool isValidColumn(vector<vector<char>>& board, int j) {
		vector<bool> visited(9, false);

		for (int i = 0; i <= 8; ++i)
			if (board[i][j] == '.') continue;
			else if (board[i][j] <= 48 || board[i][j] > 57) return false;
			else if (visited[board[i][j]-49]) return false;
			else visited[board[i][j]-49] = true;

		return true;
	}

	bool isValidBlock(vector<vector<char>>& board, int i, int j) {
		vector<bool> visited(9, false);

		for (int s = i*3; s <= i*3+2; ++s)
			for (int t = j*3; t <= j*3+2; ++t)
				if (board[s][t] == '.') continue;
				else if (board[s][t] <= 48 || board[s][t] > 57) return false;
				else if (visited[board[s][t]-49]) return false;
				else visited[board[s][t]-49] = true;

		return true;
	}
};
