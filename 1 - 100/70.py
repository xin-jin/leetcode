class Solution(object):
    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 0: return 0
        a, b = 1, 1
        for i in range(n):
            a, b = b, a+b
        return a
