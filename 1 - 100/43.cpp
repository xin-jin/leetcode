// Run Time 8ms

class Solution {
public:
    string multiply(string num1, string num2) {
		int len1 = num1.size(), len2 = num2.size(), carry = 0;
		string ans(len1+len2, '0');

		reverse(num1.begin(), num1.end());
		reverse(num2.begin(), num2.end());
		for (int i = 0; i < len1+len2; ++i) {
			for (int j = 0; j < len1; ++j) {
				if (i-j >= 0 && i-j < len2) carry += (num1[j]-'0')*(num2[i-j]-'0');
			}
			ans[i] = carry % 10 + '0';
			carry /= 10;
		}

		while (ans.back() == '0') ans.pop_back();
		reverse(ans.begin(), ans.end());
		if (ans == "") ans = "0";
		return ans;
    }
};
