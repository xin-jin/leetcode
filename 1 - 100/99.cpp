// Run time 22ms

class Solution {
public:
    void recoverTree(TreeNode* root) {
		// false means the two wrong nodes are adjacent
        if (!in_traverse(root)) swap(wrong_node->val, wrong_node_next->val);
    }

private:
	TreeNode *wrong_node = nullptr, *last = nullptr, 
		*wrong_node_next = nullptr;
    bool in_traverse(TreeNode* node) {
        if (node->left && in_traverse(node->left))
            return true;
        if (last && node->val < last->val) {
			if (wrong_node) {
				swap(wrong_node->val, node->val);
				return true;
			}
            else {
				wrong_node = last;
				wrong_node_next = node;
			}
        }
		last = node;
        if (node->right && in_traverse(node->right))
            return true;
        return false;
    }
};
