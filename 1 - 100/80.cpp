// Run Time 16ms

class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
		int cnt = 0, last = nums.size(), rep = 0, rep_val;

		for (int i = 0; i != nums.size(); ++i) {
			if (rep_val != nums[i] || !rep) {
				rep = 1;
				rep_val = nums[i];
				nums[i-cnt] = nums[i];
			}
			else if (rep == 2) ++cnt;
			else {
				nums[i-cnt] = nums[i];
				++rep;
			}
		}

		return last - cnt;
    }
};
