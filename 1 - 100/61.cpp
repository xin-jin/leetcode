// Run Time 8ms

class Solution {
public:
    ListNode* rotateRight(ListNode* head, int k) {
		ListNode *cptr;
		int size = 1;

		if (!head) return head;
		for (cptr = head; cptr->next != NULL; cptr = cptr->next, ++size);
		k %= size;
		if (!k) return head;
		cptr->next = head;
		cptr = head;
		for (int i = 0; i != size-k-1; cptr = cptr->next, ++i);
		head = cptr->next;
		cptr->next = NULL;
		
		return head;
    }
};
