// Run Time 8ms

class Solution {
public:
    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
		vector<vector<int>> ans;
		vector<int> acc;

		sort(candidates.begin(), candidates.end());
		bfs(ans, candidates, target, 0, acc);			

		return ans;
    }

private:
	void bfs(vector<vector<int>>& ans, vector<int>& can, int diff, int i, vector<int>& acc) {
		if (i == can.size() || diff < can[i]) return;
		acc.push_back(can[i]);
		if (diff == can[i]) ans.push_back(acc);
		else bfs(ans, can, diff-can[i], i+1, acc);
		acc.pop_back();
		do ++i; while(can[i] == can[i-1]);
		bfs(ans, can, diff, i, acc);
	}
};
