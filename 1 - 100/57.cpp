// Run Time 580ms

class Solution {
public:
    vector<Interval> insert(vector<Interval>& intervals, Interval newInterval) {
		int i = 0;

		if (!intervals.size()) return vector<Interval>({newInterval});
		while (i != intervals.size() && newInterval.start >= intervals[i].start) ++i;
		if (i > 0 && intervals[i-1].end >= newInterval.start) {
			if (newInterval.end <= intervals[i-1].end) return intervals;
			newInterval.start = intervals[i-1].start;
			intervals.erase(intervals.begin()+i-1);
			--i;
		}

		if (i == intervals.size()) intervals.push_back(newInterval);
		else {
			int j = i;
			while (j != intervals.size() && newInterval.end >= intervals[j].start) {
				if (newInterval.end < intervals[j].end) newInterval.end = intervals[j].end;
				++j;
			}
			intervals.erase(intervals.begin()+i, intervals.begin()+j);
			intervals.insert(intervals.begin()+i, newInterval);
		}

		return intervals;
	}
};
