// Run Time 12ms

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
		int i = 0, j = matrix.size()-1, row;
		if (j < 0) return false;
		while (i < j) {
			int mid = (i+j+1)/2;
			if (target == matrix[mid][0]) return true;
			if (target < matrix[mid][0]) j = mid - 1;
			else i = mid;
		}
		row = i; i = 0, j = matrix[0].size()-1;
		while (i <= j) {
			int mid = (i+j)/2;
			if (target == matrix[row][mid]) return true;
			if (target < matrix[row][mid]) j = mid - 1;
			else i = mid + 1;
		}

		return false;
    }
};
